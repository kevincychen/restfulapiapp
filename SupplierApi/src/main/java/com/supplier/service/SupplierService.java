package com.supplier.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.supplier.common.SupplierAdditionVo;
import com.supplier.common.SupplierEditorVo;
import com.supplier.common.SupplierListVo;
import com.supplier.dao.IPureSuppliersDao;
import com.supplier.dao.ISupplierDao;
import com.supplier.model.Suppliers;

@Service("supplierService")
@Transactional
public class SupplierService {
	
	@Autowired
	ISupplierDao iSupplierDao;
	
	public Suppliers convertVo(SupplierAdditionVo vo) {
		Suppliers supplier = new Suppliers();
		supplier.setSid(UUID.randomUUID().toString());
		supplier.setName(vo.getName());
		supplier.setType(vo.getType());
		supplier.setAddress(vo.getAddress());
		supplier.setPhone(vo.getPhone());
		supplier.setEmail(vo.getEmail());
		supplier.setDisabled(new BigDecimal("0"));
		supplier.setCompanyId(0);
		supplier.setCreateTime(new Timestamp(System.currentTimeMillis()));
		return supplier;
	}
	
	public void addSupplier(Suppliers supplier) {
		iSupplierDao.save(supplier);
	}
	
	public SupplierEditorVo convertVoForEdit(Suppliers s) {
		SupplierEditorVo vo = new SupplierEditorVo();
		vo.setSid(s.getSid());
		vo.setName(s.getName());
		vo.setType(s.getType());
		vo.setAddress(s.getAddress());
		vo.setPhone(s.getPhone());
		vo.setEmail(s.getEmail());
		return vo;
	}
	
	public SupplierEditorVo findSupplierEditorVo(String sid) {
		return convertVoForEdit(iSupplierDao.findBySid(sid).get(0));
	}
	
	@Autowired
	IPureSuppliersDao iPureSuppliersDao;
	
	public boolean updateSupplier(SupplierEditorVo vo) {
		return iPureSuppliersDao.updateSuppliers(vo);
	}
	
	public List<SupplierListVo> convertVoForList(List<Suppliers> suppliersList) {
		List<SupplierListVo> list = new ArrayList<>();
		for(Suppliers s : suppliersList) {
		    SupplierListVo vo = new SupplierListVo();
		    vo.setSid(s.getSid());
		    vo.setName(s.getName());
		    vo.setType(s.getType());
		    list.add(vo);
		}
		
		return list;		
	}
	
	public List<SupplierListVo> find(
		String keyword, int pageIndex, int size) {
		return convertVoForList(iSupplierDao.find(
			keyword, pageIndex, size));
	}
	
	public boolean delete(String sid) {
		List<Suppliers> supplierList = iSupplierDao.findBySid(sid);
		if((supplierList == null) || supplierList.isEmpty()) {
			return false;
		} else {
			Suppliers supp = supplierList.get(0);
			supp.setDisabled(new BigDecimal(1));
			iSupplierDao.save(supp);
			return true;
		}
	}

}
