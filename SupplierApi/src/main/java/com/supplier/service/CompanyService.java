package com.supplier.service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.supplier.common.BeverageCompanyVo;
import com.supplier.dao.ICompanyDao;
import com.supplier.dao.ISupplierDao;
import com.supplier.model.Companies;
import com.supplier.model.Suppliers;

@Service("companyService")
@Transactional
public class CompanyService {
    @Autowired
    private EntityManagerFactory entityManagerFactory;
    
    private static final Logger logger = 
        	LoggerFactory.getLogger(CompanyService.class);
    
    @SuppressWarnings("unchecked")
	public List<BeverageCompanyVo> getCandidateSuppliers(
    	String beverage, 
    	int pageIndex,
    	int size ){
        EntityManager session = 
        	entityManagerFactory.createEntityManager();
        try {
        	StringBuilder query = new StringBuilder("select");
        	query.append(" t.sid, t.name, t.login_id, t.company_id")
        		.append(" from ( select null as sid, (second_name || first_name) as name, login_id, company_id, create_time")
        			.append(" from x2b_users u")
        				.append(" where role_id = 'tea'")
        					.append(" and status = 1")
        						.append(" union")
        							.append(" select sid, name, null as login_id , company_id, create_time")
        								.append(" from x2b_suppliers s")
        									.append(" ) t")
        										.append(" inner join x2b_companies c on c.company_id = t.company_id and c.cname = :cname")
        											.append(" group by t.sid, t.name, t.login_id, t.company_id, t.create_time")
        												.append(" order by t.create_time desc")
        													.append(" limit :limit")
        														.append(" offset :offset");


            return (List<BeverageCompanyVo>)session
                	.createNativeQuery(query.toString())
                	.setParameter("cname", beverage)
                    .setParameter("limit", size)
                	.setParameter("offset", (pageIndex - 1) * size)
                    .getResultList();
        }
        catch (NoResultException e){
        	logger.error(e.getMessage());
            return  Collections.emptyList();
        }
        finally {
            if(session.isOpen()) session.close();
        }
    }
    
	@Autowired
	ISupplierDao iSupplierDao;
	@Autowired
	ICompanyDao iCompanyDao;
    
	public void handleBeverageSuppliers(String beverage, BeverageCompanyVo[] suppliers) {
		for (BeverageCompanyVo vo : suppliers) {
			String sid = vo.getSid();
			if (sid == null) {
				String newSid = UUID.randomUUID().toString();
				Suppliers sup = new Suppliers();
				sup.setSid(newSid);
				sup.setName(vo.getName());
				sup.setCompanyId(Long.parseLong(vo.getCompanyid()));
				iSupplierDao.save(sup);
				Companies company = new Companies();
				company.setCompanyId(Long.parseLong(vo.getCompanyid()));
				company.setCname(beverage);
				company.setSid(newSid);
				iCompanyDao.save(company);
			} else {
				Suppliers sup = iSupplierDao.findBySid(sid).get(0);
				sup.setCompanyId(Long.parseLong(vo.getCompanyid()));
				iSupplierDao.save(sup);
				Companies company = iCompanyDao.findBySid(sid).get(0);
				company.setCompanyId(Long.parseLong(vo.getCompanyid()));
				company.setCname(beverage);
				iCompanyDao.save(company);
			}
		}
	}
}
