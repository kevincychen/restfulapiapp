package com.supplier.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", 
				Pattern.CASE_INSENSITIVE);
	
	public static final Pattern VALID_CN_MOBILE_REGEX = 
			Pattern.compile("^([1][3-8][0-9]{9})$");
	public static final Pattern VALID_TW_PHONE_REGEX = 
			Pattern.compile("^(0[2-9]{1,2}-[0-9]{6,8})$");
	public static final Pattern VALID_TW_MOBILE_REGEX = 
			Pattern.compile("^(09[0-9]{8})$");
	public static final Pattern VALID_TW_PHONE_MATSU_REGEX = 
			Pattern.compile("^(0[2-9]{3}-[0-9]{5})$");
	
	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
	
	public static boolean validatePhone(String phone) {
		return VALID_CN_MOBILE_REGEX.matcher(phone).matches() ||
			   VALID_TW_PHONE_REGEX.matcher(phone).matches()  ||
			   VALID_TW_MOBILE_REGEX.matcher(phone).matches() ||
			   VALID_TW_PHONE_MATSU_REGEX.matcher(phone).matches();
	}
	
	private Validator() {
		
	}
}
