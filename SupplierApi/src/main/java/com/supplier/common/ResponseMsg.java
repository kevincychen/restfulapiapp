package com.supplier.common;

public enum ResponseMsg {
	
	//For success
	OK("OK"),
	//For 400 Bad Request
	INVALIDATE_PHONE("INVALIDATE_PHONE"),
	INVALIDATE_EMAIL("INVALIDATE_EMAIL"),
	//For supplier update
	UPDATE_FAILED("UPDATE_FAILED"),
	//For supplier delete
	DELETE_FAILED("DELETE_FAILED");
	
	private final String value;
	
	private ResponseMsg(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
