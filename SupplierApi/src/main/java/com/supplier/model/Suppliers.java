package com.supplier.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity(name = "Suppliers")
@Table(name = "x2b_suppliers")

@SqlResultSetMapping(name="updateResult", columns = { @ColumnResult(name = "count")})
@NamedNativeQueries({
	@NamedNativeQuery(
		name    =   "updateSuppliers",
        query   =   "UPDATE x2b_suppliers SET name = ?1, type = ?2 , address = ?3, phone = ?4, email = ?5 WHERE sid = ?6"
        ,resultSetMapping = "updateResult"
    )
})

public class Suppliers {
	@Id
	private String sid;
	
	@Column(length = 20)
	private String name;
	
	@Column(length = 2)
	private String type;

	@Column(length = 1024)
	private String address;
	
	@Column(length = 15)
	private String phone;
	
	@Column(length = 254)
	private String email;

	@Column(length = 2)
	private BigDecimal disabled;
	
	@Column(name ="company_id")
	private long companyId;
	
	@Column(name = "create_time",
			columnDefinition= "TIMESTAMP WITH TIME ZONE")
	private Timestamp createTime;

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getDisabled() {
		return disabled;
	}

	public void setDisabled(BigDecimal disabled) {
		this.disabled = disabled;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
}
