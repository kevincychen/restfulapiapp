package com.supplier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplier.common.AjaxResponse;
import com.supplier.common.Response;
import com.supplier.common.ResponseMsg;
import com.supplier.common.Status;
import com.supplier.common.SupplierAdditionVo;
import com.supplier.common.SupplierEditorVo;
import com.supplier.common.SupplierListVo;
import com.supplier.common.util.Validator;
import com.supplier.service.SupplierService;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
	
	@Autowired
	SupplierService supplierService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST,
			        produces = {"application/json"})
	public Response add(@RequestBody SupplierAdditionVo supplier) {
		if(!Validator.validatePhone(supplier.getPhone())) {
			return new AjaxResponse(Status.STATUS400, 
				ResponseMsg.INVALIDATE_PHONE.toString(), null);			
		} else if(!Validator.validateEmail(supplier.getEmail())) {
			return new AjaxResponse(Status.STATUS400, 
					ResponseMsg.INVALIDATE_EMAIL.toString(), null);
		} else {
		 
			supplierService.addSupplier(
				supplierService.convertVo(supplier));			
		
			return new AjaxResponse(Status.SUCCESS, 
					ResponseMsg.OK.toString(), 
					null);
		}
	}

	@RequestMapping(value = "/load/{sid}", method = RequestMethod.GET,
	                produces = {"application/json"})
	public SupplierEditorVo load(@PathVariable String sid) {
		return supplierService.findSupplierEditorVo(sid);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST,
	        produces = {"application/json"})
	public Response update(@RequestBody SupplierEditorVo vo) {
		if(!Validator.validatePhone(vo.getPhone())) {
			return new AjaxResponse(Status.STATUS400, 
				ResponseMsg.INVALIDATE_PHONE.toString(), null);			
		} else if(!Validator.validateEmail(vo.getEmail())) {
			return new AjaxResponse(Status.STATUS400, 
					ResponseMsg.INVALIDATE_EMAIL.toString(), null);
		} else {
		 
			if(supplierService.updateSupplier(vo)) {
				return new AjaxResponse(Status.SUCCESS, 
						ResponseMsg.OK.toString(), 
						null);				
			} else {
				return new AjaxResponse(Status.STATUS400, 
						ResponseMsg.UPDATE_FAILED.toString(), 
						null);				
			}
		} 
	}

	@RequestMapping(value = "/list/{string}/{num1}/{num2}", 
			method = RequestMethod.GET,
            produces = {"application/json"})
	public List<SupplierListVo> list(
			@PathVariable String string,
			@PathVariable String num1,
			@PathVariable String num2) {
		return supplierService.find(
			string,
			Integer.parseInt(num1), 
			Integer.parseInt(num2));
	}
	
	@RequestMapping(value = "/delete/{sid}",
			method = RequestMethod.POST,
            produces = {"application/json"})
	public Response delete(
			@PathVariable String sid) {
		if(supplierService.delete(sid)) {
			return new AjaxResponse(Status.SUCCESS, 
					ResponseMsg.OK.toString(), 
					null);			
		} else {
			return new AjaxResponse(Status.STATUS400, 
					ResponseMsg.DELETE_FAILED.toString(), 
					null);				
		}

	}
}
