package com.supplier.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.supplier.common.AjaxResponse;
import com.supplier.common.BeverageCompanyVo;
import com.supplier.common.Response;
import com.supplier.common.ResponseMsg;
import com.supplier.common.Status;
import com.supplier.service.CompanyService;

@RestController
public class CompanyController {

	@Autowired
	CompanyService companyService;
	
	@RequestMapping(value = "/{beverage}/supplier/{num1}/{num2}", 
			method = RequestMethod.GET,
            produces = {"application/json"})
	public List<BeverageCompanyVo> getBeverageSuppliers(
		@PathVariable String beverage,
		@PathVariable String num1,
		@PathVariable String num2) {
		return companyService.getCandidateSuppliers(
				beverage, 
				Integer.parseInt(num1), 
				Integer.parseInt(num2));
	}
	
	@RequestMapping(value = "/{beverage}/supplier", 
			method = RequestMethod.POST,
            produces = {"application/json"})
	public Response setBeverageSuppliers(
		@PathVariable String beverage,
		@RequestBody BeverageCompanyVo[] supplier) {
		companyService.handleBeverageSuppliers(
			beverage, 
			supplier); 
			return new AjaxResponse(Status.SUCCESS, 
					ResponseMsg.OK.toString(), 
					null);		
	}
}
