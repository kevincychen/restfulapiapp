package com.supplier.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.supplier.model.Suppliers;

@Repository
public interface ISupplierDao extends CrudRepository<Suppliers, String> {
	@Query(value = "select * from x2b_suppliers s where s.sid = ?1", 
		   nativeQuery = true)
	public List<Suppliers> findBySid(String sid);
	//@Query(value = "select NEW com.supplier.common.SupplierListVo(s.sid, s.name, s.type) from x2b_suppliers as s group by s.sid where s.disabled = 0 and s.name "
	@Query(value = "select * from x2b_suppliers s where s.disabled = 0 and s.name "
			+ "like %?1% order by s.create_time desc limit ?3 offset ?2", 
		   nativeQuery = true)	
	public List<Suppliers> find(String keyword, int pageIndex, int size);
}
