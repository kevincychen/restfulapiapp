package com.supplier.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.supplier.model.Companies;

@Repository
public interface ICompanyDao 
	extends CrudRepository<Companies, String> {
	@Query(value = "select * from x2b_companies c where c.sid = ?1", 
			   nativeQuery = true)
		public List<Companies> findBySid(String sid);
}
