package com.supplier.dao;

import com.supplier.common.SupplierEditorVo;

public interface IPureSuppliersDao {
	public boolean updateSuppliers(SupplierEditorVo vo);
}
