package com.supplier.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.supplier.common.SupplierEditorVo;
import com.supplier.dao.IPureSuppliersDao;
import com.supplier.model.Suppliers;

@Repository
@Transactional
public class PureSuppliersDaoImpl 
	implements IPureSuppliersDao{
	
    private static final Logger logger = 
    	LoggerFactory.getLogger(PureSuppliersDaoImpl.class);

    @PersistenceContext
    private EntityManager manager;
    
	@Override
	public boolean updateSuppliers(SupplierEditorVo vo) {
        try
        {
            manager.createNamedQuery("updateSuppliers", 
            	Suppliers.class)
            .setParameter(1, vo.getName())
            .setParameter(2, vo.getType())
            .setParameter(3, vo.getAddress())
            .setParameter(4, vo.getPhone())
            .setParameter(5, vo.getEmail())
            .setParameter(6, vo.getSid())
            .executeUpdate();
             
            return true;
        }
        catch (Exception e)
        {
        	StringBuilder msg = 
        	    new StringBuilder(e.getMessage());
        	msg.append("\n");
        	logger.error(msg.toString());
            return false;
        }
	}
}
