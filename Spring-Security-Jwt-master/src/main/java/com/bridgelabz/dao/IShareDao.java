package com.bridgelabz.dao;

import java.util.List;

import com.bridgelabz.model.Orders;


public interface IShareDao {
	
	public List<Orders> findAll();
	public int getProductPrice(String productName);
	public void updateProductPrice(String productName, int price);
	public List<Orders> findOne(int id);
}
