package com.bridgelabz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bridgelabz.page.AjaxResponse;
import com.bridgelabz.page.Response;
import com.bridgelabz.page.Status;
import com.bridgelabz.service.GenericService;

@RestController
@RequestMapping("/springjwt")
@PreAuthorize("hasAuthority('STANDARD_USER')")
public class ResourceController {
	@Autowired
	private GenericService userService;

	@RequestMapping(value = "/users/{name}")
	//@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public Response getUser(@PathVariable("name") String username) {
		System.out.println("Name got: "+username);
		String name = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println("From context: " + name);
		return new AjaxResponse(Status.SUCCESS, "", userService.findByUsername(username));
	}

	@RequestMapping(value = "/admin/users", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN_USER')")
	public Response getUsers() {
	  return new AjaxResponse(Status.SUCCESS, "", 
	    userService.findAllUsers());
	}
}
