package com.bridgelabz.service;

import java.util.List;

import com.bridgelabz.view.OrdersDTO;


public interface ShareService {
	
	public List<OrdersDTO> findAll();
	public List<OrdersDTO> findOne(int id);
}
