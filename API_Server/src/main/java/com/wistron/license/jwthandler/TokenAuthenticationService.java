package com.wistron.license.jwthandler;

import static java.util.Collections.emptyList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {

	private final static Logger LOGGER = LoggerFactory.getLogger(TokenAuthenticationService.class);

	private static String TOKEN_PREFIX = "Bearer"; // the prefix of the token in the http header
	public static String HEADER_STRING = "Authorization"; // the http header containing the prexif + the token
	private static String DEFAULT_SECRET = "secret";

	public static Authentication getAuthentication(String token) {
		if (token != null && token.startsWith(TOKEN_PREFIX)) {
			String content = null;
			String compactJws = token.replace(TOKEN_PREFIX, "");
			String envTokenSecret = System.getenv("TOKENSECRET");
			String signingKey = (envTokenSecret == null || envTokenSecret.isEmpty()) ? DEFAULT_SECRET : envTokenSecret;
			LOGGER.debug("[TokenAuthenticationService][getAuthentication] compactJws = {}, signingKey = {}", compactJws,
					signingKey);
			try {
				content = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(compactJws).getBody().getSubject();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (content != null && validateTokenContent(content)) {
				return new UsernamePasswordAuthenticationToken(content, null, emptyList());
			} else {
				return null;
			}
		}
		LOGGER.debug("[TokenAuthenticationService][getAuthentication] no token");
		return null;
	}

	private static boolean validateTokenContent(String content) {
		boolean isValidatedOK = false;
		String projectId = System.getenv("PROJECT_ID");
		if (StringUtils.isNoneBlank(projectId) && projectId.equals(content)) {
			isValidatedOK = true;
		}
		return isValidatedOK;
	}

	public static String genAuthenticationJWT(String projectID) {
		Date now = new Date();
		String jwt = Jwts.builder().setId(projectID).setSubject(projectID).setIssuedAt(now)
				.signWith(SignatureAlgorithm.HS256, DEFAULT_SECRET).compact();
		return jwt;
	}

	public static boolean saveJWT(String jwt, String tokenFilePath) {
		boolean isSaveSuccess = false;
		PrintWriter printWriter = null;
		try {
			File file = new File(tokenFilePath);
			FileUtils.writeStringToFile(file, jwt, "UTF-8");
			isSaveSuccess = true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (printWriter != null) {
				printWriter.close();
			}
		}
		return isSaveSuccess;
	}

	public static String getJWT(String tokenFilePath) {
		if (tokenFilePath == null) {
			LOGGER.error("[TokenAuthenticationService][getJWT] tokenFilePath is null");
		    return null;
		}
		
		File file = new File(tokenFilePath);
		if (!file.exists()) {
			LOGGER.error("[TokenAuthenticationService][getJWT] not exist {}", tokenFilePath);
		    return null;
		}
		
		String JWT = null;
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(tokenFilePath));
			StringBuffer stringBuffer = new StringBuffer();
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
			}
			JWT = stringBuffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				IOUtils.closeQuietly(bufferedReader);
			} 
		}
		return JWT;
	}


	public static boolean createAuthenticationJWT(String projectID, String tokenFilePath) {
		String jwt = genAuthenticationJWT(projectID);
		return saveJWT(jwt, tokenFilePath);
	}
	
	public static boolean deleteAuthenticationJWT(String tokenFilePath) {
		File zipFile = new File(tokenFilePath);
		return zipFile.delete();
	}	
	
}