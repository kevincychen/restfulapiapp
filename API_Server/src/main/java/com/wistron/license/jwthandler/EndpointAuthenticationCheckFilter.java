package com.wistron.license.jwthandler;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

import com.wistron.license.jwthandler.TokenAuthenticationService;

public class EndpointAuthenticationCheckFilter extends GenericFilterBean {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(EndpointAuthenticationCheckFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    	
    	LOGGER.debug("[EndpointAuthenticationCheckFilter][attemptAuthentication] Start");
    	
    	HttpServletRequest httpRequest = (HttpServletRequest) request;
		Authentication authentication = TokenAuthenticationService.getAuthentication(httpRequest.getHeader(TokenAuthenticationService.HEADER_STRING));
		
		// Apply the authentication to the SecurityContextHolder
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		// Go on processing the request
		filterChain.doFilter(request, response);
		
		// Clears the context from authentication
		SecurityContextHolder.getContext().setAuthentication(null);        
    }
}
