
package com.wistron.license.service;

import java.io.IOException;

import com.wistron.license.common.EmailContextVO;
import com.wistron.license.common.EmailInfoVO;
import com.wistron.license.common.EmailResponseVO;

public interface EmailService {
	// ================================================
	// == [Method] Block Start
	// ====
	public EmailInfoVO getMailInfo() throws IOException;
	
	public EmailResponseVO testSendEmail(EmailInfoVO emailInfoVO);
	
	public EmailResponseVO saveEmailInfo(EmailInfoVO emailInfoVO);
	
	public EmailResponseVO sendEmailFromFile(EmailContextVO emailContextVO) throws IOException;

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
