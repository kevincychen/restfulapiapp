
package com.wistron.license.service.impl;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.wistron.license.common.EmailContextVO;
import com.wistron.license.common.EmailInfoVO;
import com.wistron.license.common.EmailResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.util.EmailUtils;
import com.wistron.license.common.util.ReaderAndWriterFileUtils;
import com.wistron.license.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	private final static Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	// @Value("${mail.host}")
	// private String host;
	//
	// @Value("${mail.smtp.port}")
	// private Integer port;
	//
	// @Value("${mail.senderEmail}")
	// private String senderEmail;
	//
	// @Value("${mail.senderPassword}")
	// private String senderPassword;
	//
	// @Value("${mail.recipients}")
	// private List<String> recipients;

	@Value("${mail.file}")
	private String mailPath;

	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	/**
	 * 實作「 getMailInfo 」方法
	 * 
	 * @throws IOException
	 * @see com.wistron.license.service.EmailService#getMailInfo()
	 */
	@Override
	public EmailInfoVO getMailInfo() throws IOException {
		LOGGER.info("[EmailServiceImpl][getMailInfo] mailPath ={}", mailPath);

		EmailInfoVO emailInfoVO;
		String fileStr = ReaderAndWriterFileUtils.readerFile(mailPath);
		if (StringUtils.isBlank(fileStr)) { 
			emailInfoVO = new EmailInfoVO();
		} else {
			emailInfoVO = new Gson().fromJson(fileStr, EmailInfoVO.class);
		}
		return emailInfoVO;
	}

	/**
	 * 實作「 testSendEmail 」方法
	 * 
	 * @see com.wistron.license.service.EmailService#testSendEmail(emailInfoVO)
	 */
	@Override
	public EmailResponseVO testSendEmail(EmailInfoVO emailInfoVO) {
		LOGGER.info("[EmailServiceImpl][testSendEmail]");
		if (!this.isEmailInfoVOValid(emailInfoVO)) {
			return new EmailResponseVO(false, ResponseMsg.ARGUMENTS_ERROR.toString());
		}
		EmailContextVO emailContextVO = new EmailContextVO("test SignFaceID mail", "Sending mail is successful.");
		EmailResponseVO emailResponseVO = EmailUtils.sendMail(emailContextVO, emailInfoVO);
		return emailResponseVO;
	}

	/**
	 * 實作「 saveEmailInfo 」方法
	 * 
	 * @see com.wistron.license.service.EmailService#testSendEmail(emailInfoVO)
	 */
	@Override
	public EmailResponseVO saveEmailInfo(EmailInfoVO emailInfoVO) {
		LOGGER.info("[EmailServiceImpl][saveEmailInfo]emailInfoVO = {}", emailInfoVO);
		if (!this.isEmailInfoVOValid(emailInfoVO)) {
			return new EmailResponseVO(false, ResponseMsg.ARGUMENTS_ERROR.toString());
		}
		String emailInfoStr = new Gson().toJson(emailInfoVO);
		EmailResponseVO EmailResponseVO = ReaderAndWriterFileUtils.writerFile(emailInfoStr, mailPath);
		return EmailResponseVO;
	}

	/**
	 * 實作「 sendEmailFromFile 」方法
	 * 
	 * @throws IOException
	 * @see com.wistron.license.service.EmailService#sendEmailFromFile()
	 */
	@Override
	public EmailResponseVO sendEmailFromFile(EmailContextVO emailContextVO) throws IOException {
		LOGGER.info("[EmailServiceImpl][sendEmailFromFile]");
		EmailInfoVO emailInfoVO = this.getMailInfo();
		EmailResponseVO emailResponseVO = EmailUtils.sendMail(emailContextVO, emailInfoVO);
		return emailResponseVO;
	}

	private boolean isEmailInfoVOValid(EmailInfoVO emailInfoVO) {
		if ((StringUtils.isNotBlank(emailInfoVO.getHost()) && emailInfoVO.getHost().length() > 50)
				|| (emailInfoVO.getPort() != null && emailInfoVO.getPort() > 9999)
				|| (StringUtils.isNotBlank(emailInfoVO.getSenderEmail())
						&& emailInfoVO.getSenderEmail().length() > 100)
				|| (StringUtils.isNotBlank(emailInfoVO.getSenderPassword())
						&& emailInfoVO.getSenderPassword().length() > 50)
				|| (StringUtils.isNotBlank(emailInfoVO.getRecipients())
						&& emailInfoVO.getRecipients().length() > 1000)) {
			return false;
		}
		return true;
	}

	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
