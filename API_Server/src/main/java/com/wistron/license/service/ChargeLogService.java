package com.wistron.license.service;

import java.util.Date;

import com.wistron.license.common.CountingLogResponseVO;
import com.wistron.license.common.PrepaidLogResponseVO;

public interface ChargeLogService {

	CountingLogResponseVO queryCountingLog(Date from, Date end);

	PrepaidLogResponseVO queryPrepaidLog();

}
