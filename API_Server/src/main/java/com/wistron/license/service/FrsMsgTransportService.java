
package com.wistron.license.service;

import java.io.IOException;

import com.wistron.license.common.FrsRequestVO;

public interface FrsMsgTransportService {
	//================================================
	//== [Method] Block Start
	//====
	
	public String sendFrsMsg(FrsRequestVO requestVO) throws IOException;
	//####################################################################
	//## [Method] sub-block :
	//####################################################################
	//====
	//== [Method] Block Stop
	//================================================
}

