
package com.wistron.license.service;

import com.wistron.license.common.MigrationRequestVO;
import com.wistron.license.common.MigrationResponseVO;

public interface MigrationService {
	// ================================================
	// == [Method] Block Start
	// ====
	public MigrationResponseVO getMigrationInfo();

	public MigrationResponseVO migrate(MigrationRequestVO requestVO);
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
