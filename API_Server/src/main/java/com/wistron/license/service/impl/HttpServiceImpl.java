
package com.wistron.license.service.impl;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.service.HttpService;

/**
 *
 * Page/Class Name: HttpServiceImpl Title: Description: Copyright: Copyright(c)
 * 2004 Wistron Corportion. All Rights Reserved. I Company: Wistron Corportion
 * author: louis Create Date: 2018年2月3日 Last Modifier: louis Last Modify Date:
 * 2018年2月3日 Version 1.0
 *
 */
@Service
public class HttpServiceImpl implements HttpService {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private final String SERVER_URL = (System.getenv("SERVERURL") != null) ? System.getenv("SERVERURL")
			: "https://1jun5hdpmj.execute-api.us-east-1.amazonaws.com/dev";

	private CloseableHttpClient httpClient = HttpClients.createDefault();

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	@Override
	public String sendCodeToServer(ActivationInfoVO requestVO) throws ClientProtocolException, IOException {
		String api = SERVER_URL + "/activate";
		HttpPost request = new HttpPost(api);
		final Gson gson = new Gson();
		StringEntity params;
		params = new StringEntity(gson.toJson(requestVO));
		request.addHeader("content-type", "application/json");
		request.setEntity(params);

		CloseableHttpResponse response = httpClient.execute(request);
		String result = EntityUtils.toString(response.getEntity());
		httpClient.close();
		return result;
	}
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
