package com.wistron.license.service.impl;

import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jna.Native;
import com.wistron.license.securityModule.util.ChargeLog;
import com.wistron.license.securityModule.util.PurchaseHistory;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule;
import com.wistron.license.securityModule.util.UsageSign;
import com.wistron.license.service.EncryptService;

@Service("encryptService")
public class EncryptServiceImpl implements EncryptService {

	private final static Logger LOGGER = LoggerFactory.getLogger(EncryptServiceImpl.class);

	private SecurityModule securityModule = null;

	@Value("${lib.file}")
	private String libFile;

	@Autowired
	private Environment environment;

	private boolean load = true;

	private String env = "";

	@PostConstruct
	public void init() {
		if (this.environment.getActiveProfiles() != null && this.environment.getActiveProfiles().length == 2) {
			load = new Boolean(this.environment.getActiveProfiles()[1]);

		}

		if (load) {
			if (this.environment.getActiveProfiles() != null && this.environment.getActiveProfiles().length > 0) {
				env = this.environment.getActiveProfiles()[0];
			}
			try {
				if (StringUtils.equals("dev", this.env)) {
					this.securityModule = (SecurityModule) Native
							.loadLibrary(System.getProperty("user.dir") + "/" + libFile, SecurityModule.class);
				} else {
					this.securityModule = (SecurityModule) Native.loadLibrary(libFile, SecurityModule.class);
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}

		}
	}

	@Override
	public String getUniqueId() {
		if (load) {
			String uniqueId = SecurityModule.GolibUtils.gstr2jstr(this.securityModule.getUniqueId());
			return uniqueId;
		} else {
			return UUID.randomUUID().toString();
		}
	}

	@Override
	public boolean isActivated() {
		if (load) {
			return securityModule.isActivated() == 0 ? false : true;
		} else {
			return true;
		}
	}

	@Override
	public boolean initModule(String code, String SNSecret) {
		if (load) {
			return this.securityModule.initModule(SecurityModule.GolibUtils.jstr2gstr(code),
					SecurityModule.GolibUtils.jstr2gstr(SNSecret)) == 0 ? false : true;
		} else {
			return true;
		}

	}

	@Override
	public long getUsageCount() {
		if (load) {
			return this.securityModule.getUsageCount();
		} else {
			return 10;
		}

	}

	@Override
	public long addPurchase(String SerialNumber) {
		if (load) {
			return this.securityModule.addPurchase(SecurityModule.GolibUtils.jstr2gstr(SerialNumber));
		} else {
			return 100;
		}

	}

	@Override
	public PurchaseHistory getPurchaseList() {
		if (load) {
			final String resultStr = SecurityModule.GolibUtils.gstr2jstr(this.securityModule.getPurchaseList());
			if (StringUtils.isNotEmpty(resultStr)) {
				Gson gson = new Gson();
				return gson.fromJson(resultStr, PurchaseHistory.class);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public UsageSign getUsageSign(String id, String faceImage) {
		if (load) {
			final String resultStr = SecurityModule.GolibUtils
					.gstr2jstr(this.securityModule.getUsageSign(SecurityModule.GolibUtils.jstr2gstr(id), //
							SecurityModule.GolibUtils.jstr2gstr(faceImage)));
			if (StringUtils.isNotEmpty(resultStr)) {
				Gson gson = new Gson();
				return gson.fromJson(resultStr, UsageSign.class);
			} else {
				return null;
			}
		} else {

			UsageSign usageSign = new UsageSign();
			usageSign.setCount(100);
			usageSign.setSign(
					"08d0be3b415a59719e202eefd476cd9c66c8466a41bfad96ed9cdbb3e75ac388d665ba7d541ff6072925af9e7c7856f37282130e20a084e9d5d87d8e2e31afb9");
			return usageSign;
		}

	}

	// @Override
	// public boolean charge(String sign) {
	// if (load) {
	// return this.securityModule.charge(SecurityModule.GolibUtils.jstr2gstr(sign))
	// == 0 ? false : true;
	// } else {
	// return true;
	// }
	// }
	//
	@Override
	public ChargeLog getChargebackHistory(long start, long end) {
		if (load) {

			final String resultStr = SecurityModule.GolibUtils
					.gstr2jstr(this.securityModule.getChargebackHistory(start, end));
			if (StringUtils.isNotEmpty(resultStr)) {
				Gson gson = new Gson();
				return gson.fromJson(resultStr, ChargeLog.class);
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	@Override
	public String getMigrationInfo() {

		if (load) {
			try {
				final String resultStr = SecurityModule.GolibUtils.gstr2jstr(this.securityModule.getMigrateInfo());
				if (StringUtils.isNotEmpty(resultStr)) {
					return resultStr;
				} else {
					return StringUtils.EMPTY;
				}
			} catch (Exception e) {
				throw e;
			}
		} else {
			return StringUtils.EMPTY;

		}
	}

	@Override
	public boolean migrate(String migrateCode) {
		if (load) {
			try {

				return this.securityModule.migrate(SecurityModule.GolibUtils.jstr2gstr(migrateCode)) == 0 ? false
						: true;
			} catch (Exception e) {
				throw e;
			}
		} else {
			return true;
		}
	}

}
