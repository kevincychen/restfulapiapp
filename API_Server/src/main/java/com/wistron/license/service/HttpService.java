
package com.wistron.license.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.wistron.license.common.ActivationInfoVO;

public interface HttpService {
	//================================================
	//== [Method] Block Start
	//====
	String sendCodeToServer(ActivationInfoVO requestVO) throws ClientProtocolException, IOException;
	//####################################################################
	//## [Method] sub-block :
	//####################################################################
	//====
	//== [Method] Block Stop
	//================================================
}

