
package com.wistron.license.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.wistron.license.common.EmailContextVO;
import com.wistron.license.common.FrsAction;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResponseVO;
import com.wistron.license.common.FrsResult;
import com.wistron.license.common.FrsStatus;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.util.DateUtils;
import com.wistron.license.common.util.FileVO;
import com.wistron.license.common.util.Validator;
import com.wistron.license.securityModule.util.UsageSign;
import com.wistron.license.service.EmailService;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.FrsMsgTransportService;
import com.wistron.license.service.FrsService;
import com.wistron.license.service.LogService;

@Service
public class FrsServiceImpl implements FrsService {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	private final static Logger LOGGER = LoggerFactory.getLogger(FrsServiceImpl.class);
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====

	@Value("${log_path}")
	private String logPath;

	@Value("${temp_path}")
	private String tempPath;

	@Value("${file.limit}")
	private double fileLimit;

	@Autowired
	private EncryptService encryptService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private LogService logService;

	@Autowired
	private FrsMsgTransportService frsMsgTransportService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====

	@Override
	public FrsResponseVO enroll(FrsRequestVO requestVO) {
		FrsResponseVO responseVO = new FrsResponseVO();
		if (StringUtils.isEmpty(requestVO.getUserID()) || StringUtils.isEmpty(requestVO.getFaceImage())
				|| requestVO.getUserID().length() > 20 || !Validator.validateAplhabetAndNumber(requestVO.getUserID())) {
			responseVO.setStatus(FrsStatus.PARAMS_ERROR.getValue());
			return responseVO;
		}

		if (requestVO.getFaceImage().length() * 0.75 >= this.fileLimit) {
			responseVO.setStatus(FrsStatus.FILE_TOO_LARGE.getValue());
			return responseVO;

		}

		FrsResult frsResult = FrsResult.SUCCESS;
		if (this.encryptService.isActivated()) {
			final UsageSign usageSign = this.encryptService.getUsageSign(requestVO.getUserID(),
					StringEscapeUtils.unescapeJava(requestVO.getFaceImage()));
			// 確認是否已啟用
			try {
				requestVO.setFaceImage(this.saveToFile(usageSign, requestVO));

				final String result = frsMsgTransportService.sendFrsMsg(requestVO);
				final Gson gson = new Gson();
				responseVO = gson.fromJson(result, FrsResponseVO.class);
				LOGGER.debug("response vo {}", responseVO.toString());
				if (responseVO.getStatus() != 0 && responseVO.getStatus() != -1) {
					throw new Exception("enroll error");
				}

				// FileUtils.forceDelete(new File(requestVO.getFaceImage()));
			} catch (Exception e) {
				LOGGER.error("enroll error", e);
				frsResult = FrsResult.FAILE;
				responseVO.setMessage(ResponseMsg.FRS_TIMEOUT.toString());

			}

		} else {
			// 沒有啟用
			logService.logFrsAccess(requestVO, FrsAction.ENROLL, FrsResult.NOT_ACTIVATED);
			responseVO.setStatus(FrsStatus.NOT_ACTIVATED.getValue());
			frsResult = FrsResult.NOT_ACTIVATED;
		}
		logService.logFrsAccess(requestVO, FrsAction.ENROLL, frsResult);

		return responseVO;

	}

	@Override
	public FrsResponseVO recognition(FrsRequestVO requestVO) {
		FrsResponseVO responseVO = new FrsResponseVO();
		if (StringUtils.isEmpty(requestVO.getUserID()) || StringUtils.isEmpty(requestVO.getFaceImage())) {
			responseVO.setStatus(FrsStatus.PARAMS_ERROR.getValue());
			return responseVO;
		}

		if (requestVO.getFaceImage().length() * 0.75 >= this.fileLimit) {
			responseVO.setStatus(FrsStatus.FILE_TOO_LARGE.getValue());
			return responseVO;

		}

		FrsResult frsResult = FrsResult.SUCCESS;
		final UsageSign usageSign = this.encryptService.getUsageSign(requestVO.getUserID(), requestVO.getFaceImage());

		if (this.encryptService.isActivated()) {
			// 確認是否已啟用
			if (usageSign != null && usageSign.getCount() > 0) {
				// 確認是否有餘額
				try {

					requestVO.setFaceImage(this.saveToFile(usageSign, requestVO));
					LOGGER.debug("usage sign :{}", usageSign.toString());
					if (usageSign.getCount() == 500) {
						EmailContextVO emailContextVO = new EmailContextVO("Purchase Alarm",
								"Your balance is lower 500. ");
						this.emailService.sendEmailFromFile(emailContextVO);
					}
					final String result = frsMsgTransportService.sendFrsMsg(requestVO);
					final Gson gson = new Gson();
					responseVO = gson.fromJson(result, FrsResponseVO.class);
					LOGGER.debug("response vo {}", responseVO.toString());
					if (responseVO.getStatus() != 0 && responseVO.getStatus() != -1) {
						throw new Exception("recognition error");
					}

					// FileUtils.forceDelete(new File(requestVO.getFaceImage()));
				} catch (Exception e) {
					LOGGER.error("recognition error", e);
					frsResult = FrsResult.FAILE;
					responseVO.setMessage(ResponseMsg.FRS_TIMEOUT.toString());

				}
			} else {
				// 沒有餘額
				responseVO.setStatus(FrsStatus.NOT_ENOUGH.getValue());
				frsResult = FrsResult.NOT_ENOUGH;
			}

		} else {
			// 沒有啟用
			LOGGER.info("server not activated!!");
			frsResult = FrsResult.NOT_ACTIVATED;
			responseVO.setStatus(FrsStatus.NOT_ACTIVATED.getValue());
		}
		logService.logFrsAccess(requestVO, FrsAction.RECOGNITION, frsResult);
		return responseVO;

	}

	@Override
	public FrsResponseVO delete(FrsRequestVO requestVO) {
		FrsResponseVO responseVO = new FrsResponseVO();
		if (StringUtils.isEmpty(requestVO.getUserID())) {
			responseVO.setStatus(FrsStatus.PARAMS_ERROR.getValue());
			return responseVO;
		}
		FrsResult frsResult = FrsResult.SUCCESS;
		if (this.encryptService.isActivated()) {
			// 確認是否已啟用
			try {
				final String result = frsMsgTransportService.sendFrsMsg(requestVO);
				final Gson gson = new Gson();
				responseVO = gson.fromJson(result, FrsResponseVO.class);
				if (responseVO.getStatus() != 0 && responseVO.getStatus() != -1) {
					throw new Exception("delete error");
				}

			} catch (Exception e) {
				LOGGER.error("delete error", e);
				frsResult = FrsResult.FAILE;
				responseVO.setMessage(ResponseMsg.FRS_TIMEOUT.toString());
			}

		} else {
			// 沒有啟用

			frsResult = FrsResult.NOT_ACTIVATED;
			responseVO.setStatus(FrsStatus.NOT_ACTIVATED.getValue());
		}
		logService.logFrsAccess(requestVO, FrsAction.DELETE, frsResult);
		return responseVO;

	}

	private String saveToFile(UsageSign usageSign, FrsRequestVO requesVO) throws IOException {

		String uuid = StringUtils.replaceAll(UUID.randomUUID().toString(), "-", "");
		String filePath = this.tempPath + DateUtils.getDateStr(new Date(), "yyyyMMddHHmmss") + "-"
				+ requesVO.getUserID() + "-" + uuid;

		if (usageSign != null) {
			final FileVO fileVO = new FileVO(usageSign.getSign(), requesVO.getFaceImage(), requesVO.getAction(),
					requesVO.getUserID());
			Gson gson = new Gson();
			FileUtils.writeStringToFile(new File(filePath), StringEscapeUtils.unescapeJava(gson.toJson(fileVO)),
					"UTF-8");
		} else {
			FileUtils.writeStringToFile(new File(filePath), StringEscapeUtils.unescapeJava(requesVO.getFaceImage()),
					"UTF-8");
		}

		return filePath;
	}
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
