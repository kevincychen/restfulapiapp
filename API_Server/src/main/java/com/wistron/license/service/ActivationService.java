
package com.wistron.license.service;

import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.common.DefaultResponseVO;
import com.wistron.license.common.PrepaidInfoVO;
import com.wistron.license.common.PrepaidResponseVO;
import com.wistron.license.common.StatusResponseVO;
import com.wistron.license.common.TokenResponseVO;

public interface ActivationService {
	DefaultResponseVO activate(ActivationInfoVO requestVO);

	PrepaidResponseVO prepaid(PrepaidInfoVO requestVO);

	TokenResponseVO getToken();
	
	StatusResponseVO getStatus();
	
	
}
