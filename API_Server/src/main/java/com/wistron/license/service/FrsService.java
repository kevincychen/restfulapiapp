
package com.wistron.license.service;

import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResponseVO;

public interface FrsService {
	// ================================================
	// == [Method] Block Start
	// ====
	public FrsResponseVO enroll(FrsRequestVO requestVO);

	public FrsResponseVO recognition(FrsRequestVO requestVO);

	public FrsResponseVO delete(FrsRequestVO requestVO);

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
