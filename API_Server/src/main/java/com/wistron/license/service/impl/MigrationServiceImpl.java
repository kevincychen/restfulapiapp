
package com.wistron.license.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wistron.license.common.MigrationRequestVO;
import com.wistron.license.common.MigrationResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.MigrationService;

@Service
public class MigrationServiceImpl implements MigrationService {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	private final static Logger LOGGER = LoggerFactory.getLogger(MigrationServiceImpl.class);
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Autowired
	private EncryptService encryptService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	/**
	 * 實作「 TODO 」方法
	 *
	 * 
	 * @see com.wistron.license.service.MigrationService#getMigrationInfo()
	 */
	@Override
	public MigrationResponseVO getMigrationInfo() {
		MigrationResponseVO responseVO = new MigrationResponseVO();
 
		try {
			responseVO.setMigrateCode(this.encryptService.getMigrationInfo());
			responseVO.setProjectId(System.getenv("PROJECT_ID"));
			responseVO.setMessage(ResponseMsg.OK.toString());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			responseVO.setMessage(ResponseMsg.SERVER_ERROR.toString());
		}
		return responseVO;

	}

	@Override
	public MigrationResponseVO migrate(MigrationRequestVO requsetVO) {
		boolean result = false;
		MigrationResponseVO responseVO = new MigrationResponseVO();
		if (!StringUtils.isEmpty(requsetVO.getMigrateCode())) {
			try {
				result = this.encryptService.migrate(requsetVO.getMigrateCode());
				responseVO.setMessage(ResponseMsg.OK.toString());
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				responseVO.setMessage(ResponseMsg.SERVER_ERROR.toString());
			}
		} else {
			responseVO.setMessage(ResponseMsg.ARGUMENTS_ERROR.toString());
		}
		responseVO.setMigrateResult(new Boolean(result));
		return responseVO;
	}

	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
