
package com.wistron.license.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wistron.license.common.FrsAction;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResult;
import com.wistron.license.common.LogResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.util.DateUtils;
import com.wistron.license.logger.util.LogVO;
import com.wistron.license.logger.util.LogbackFileUtils;
import com.wistron.license.service.LogService;

@Service
public class LogServiceImpl implements LogService {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	private static Logger FRS_LOGGER = LoggerFactory.getLogger(LogbackFileUtils.FRS_LOGGER);
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private int limit = 1000;

	@Value("${log_path}")
	private String logPath;

	@Value("${temp_path}")
	private String tempPath;
	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====

	@Override
	public void logFrsAccess(FrsRequestVO requestVO, FrsAction frsAction, FrsResult frsResult) {

		final String dateStr = DateUtils.getTodayStr("yyyyMMdd");
		LogbackFileUtils.start(logPath + "frs/" + dateStr + ".csv");
		FRS_LOGGER.info("{},{},{}", requestVO.getUserID(), frsAction, frsResult);
		LogbackFileUtils.stop();

	}

	@Override
	public LogResponseVO getLog(int actions[], Date from, Date end) {
		if (from.after(end)) {
			return new LogResponseVO(false, null, ResponseMsg.ARGUMENTS_ERROR.toString());
		}

		final List<String> frsActions = this.transferFrsAction(actions);

		if (CollectionUtils.isEmpty(frsActions)) {
			return new LogResponseVO(false, null, ResponseMsg.ARGUMENTS_ERROR.toString());
		}

		try {
			final String format = "yyyyMMdd";
			int fromNum = new Integer(DateUtils.getDateStr(from, format));
			int endNum = new Integer(DateUtils.getDateStr(end, format));
			final List<LogVO> results = new ArrayList<LogVO>();
			int count = 0;
			LogResponseVO responseVO = null;
			while (endNum >= fromNum) {
				results.addAll(this.readLogFile(logPath + "frs/" + String.valueOf(endNum) + ".csv", frsActions, count));
				count = results.size();
				if (count >= limit) {
					// 超過最大值
					responseVO = new LogResponseVO(true, results, ResponseMsg.OK.toString());
					break;
				}
				endNum = new Integer(DateUtils.addDay(String.valueOf(endNum), format, -1));

			}
			if (responseVO == null) {
				responseVO = new LogResponseVO(false, results, ResponseMsg.OK.toString());
			}
			return responseVO;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new LogResponseVO(false, null, ResponseMsg.SERVER_ERROR.toString());
	}

	@Override
	public String exportLog(final Date from, final Date end) {
		if (from.after(end))
			return ResponseMsg.ARGUMENTS_ERROR.toString();

		try {
			final String format = "yyyyMMdd";
			int fromNum = new Integer(DateUtils.getDateStr(from, format));
			int endNum = new Integer(DateUtils.getDateStr(end, format));
			final List<String> logs = new ArrayList<String>();
			while (fromNum <= endNum) {
				final String filePath = logPath + "frs/" + String.valueOf(fromNum) + ".csv";
				File file = new File(filePath);
				if (file.exists()) {
					logs.add(filePath);
				}
				fromNum = new Integer(DateUtils.addDay(String.valueOf(fromNum), format, 1));
			}
			return this.zipFile(logs);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseMsg.SERVER_ERROR.toString();
	}

	private List<LogVO> readLogFile(final String path, final List<String> actions, int count) throws IOException {
		final List<LogVO> results = new ArrayList<LogVO>();
		File file = new File(path);
		if (file.exists()) {

			try (ReversedLinesFileReader reader = new ReversedLinesFileReader(file, Charset.forName("UTF-8"))) {
				String line;

				while ((line = reader.readLine()) != null) {
					if (count >= limit) {
						break;
					}
					final LogVO logVO = LogVO.getInstance(StringUtils.split(line, ","), actions);
					if (logVO != null) {
						results.add(logVO);
					}
					count++;
				}

			}

		}
		return results;

	}

	private List<String> transferFrsAction(final int[] actions) {
		final List<String> frsActions = new ArrayList<String>();
		for (int action : actions) {
			FrsAction frsAction = FrsAction.lookUp(action);
			if (frsAction != null) {
				frsActions.add(frsAction.toString());
			}
		}
		return frsActions;
	}

	private String zipFile(final List<String> logs) throws IOException {
		File file = new File(tempPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		final String fileName = DateUtils.getDateStr(new Date(), "yyMMddHHmmss") + ".zip";
		final String zipFile = "temp/" + fileName;
		try {

			byte[] buffer = new byte[1024];
			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			for (String log : logs) {
				File srcFile = new File(log);
				FileInputStream fis = new FileInputStream(srcFile);
				zos.putNextEntry(new ZipEntry(srcFile.getName()));
				int length;
				while ((length = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, length);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();

		} catch (IOException ioe) {

			throw ioe;

		}
		return fileName;

	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
