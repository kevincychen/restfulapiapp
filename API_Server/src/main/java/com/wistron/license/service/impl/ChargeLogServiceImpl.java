package com.wistron.license.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wistron.license.common.CountingLogResponseVO;
import com.wistron.license.common.PrepaidLogResponseVO;
import com.wistron.license.common.PrepaidLogVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.util.DateUtils;
import com.wistron.license.securityModule.util.ChargeLog;
import com.wistron.license.securityModule.util.Log;
import com.wistron.license.securityModule.util.Purchase;
import com.wistron.license.securityModule.util.PurchaseHistory;
import com.wistron.license.service.ChargeLogService;
import com.wistron.license.service.EncryptService;

@Service
public class ChargeLogServiceImpl implements ChargeLogService {

	private final static Logger LOGGER = LoggerFactory.getLogger(ChargeLogServiceImpl.class);

	@Autowired
	private EncryptService encryptService;

	@Override
	public CountingLogResponseVO queryCountingLog(Date from, Date end) {
		if (from.after(end)) {
			return new CountingLogResponseVO(false, null, ResponseMsg.ARGUMENTS_ERROR);
		}
 
		List<String> lngDate = new ArrayList<String>();
		CountingLogResponseVO responseVO = new CountingLogResponseVO(false, lngDate, ResponseMsg.OK);
 
		try {
			ChargeLog chargeLog = this.encryptService.getChargebackHistory(DateUtils.getDateStart(from).getTime(),
					DateUtils.getDateEnd(end).getTime());
			if (chargeLog != null && CollectionUtils.isNotEmpty(chargeLog.getList())) {
				for (Log log : chargeLog.getList()) {
					lngDate.add(DateUtils.getDateStr(log.getTimestamp(), "yyyy-MM-dd HH:mm:ss.SSS"));
				}
				responseVO = new CountingLogResponseVO(false, lngDate, ResponseMsg.OK);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			responseVO.setMessage(ResponseMsg.SERVER_ERROR.toString());
		}

		return responseVO;
	}

	@Override
	public PrepaidLogResponseVO queryPrepaidLog() {
		List<PrepaidLogVO> tmpDataList = new ArrayList<PrepaidLogVO>();
		PrepaidLogResponseVO responseVO = new PrepaidLogResponseVO(false, tmpDataList, ResponseMsg.OK.toString());
		try {
			final PurchaseHistory history = this.encryptService.getPurchaseList();
			if (history != null && CollectionUtils.isNotEmpty(history.getList())) {

				for (Purchase purchase : history.getList()) {
					tmpDataList.add(new PrepaidLogVO(purchase));
				}
				responseVO = new PrepaidLogResponseVO(false, tmpDataList, ResponseMsg.OK.toString());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			responseVO.setMessage(ResponseMsg.SERVER_ERROR.toString());
		}

		return responseVO;
	}

}
