
package com.wistron.license.service;
import java.util.Date;

import com.wistron.license.common.FrsAction;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResult;
import com.wistron.license.common.LogResponseVO;

/**
 *
 * Page/Class Name: LogService Title: Description: Copyright: Copyright(c) 2004
 * Wistron Corportion. All Rights Reserved. I Company: Wistron Corportion
 * author: louis Create Date: 2018年1月11日 Last Modifier: louis Last Modify Date:
 * 2018年1月11日 Version 1.0
 *
 */
public interface LogService {
	// ================================================
	// == [Method] Block Start
	// ====
	LogResponseVO getLog(int[] actions, Date from, Date end);

	String exportLog(Date from, Date end);

	void logFrsAccess(FrsRequestVO requestVO, FrsAction frsAction, FrsResult frsResult);
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
