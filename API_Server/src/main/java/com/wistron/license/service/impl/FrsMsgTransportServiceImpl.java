
package com.wistron.license.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.service.FrsMsgTransportService;
import com.wistron.license.udp.UdpClient;

@Service
public class FrsMsgTransportServiceImpl implements FrsMsgTransportService {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Value("${frs.port}")
	private String frsPort;

	@Value("${frs.ip}")
	private String frsIp;

	@Value("${frs.timeout}")
	private String frsTimeout;

	private UdpClient udpClient = new UdpClient();

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	@Override
	public String sendFrsMsg(FrsRequestVO requestVO) throws IOException {

		udpClient.setInfo(frsIp, new Integer(frsPort), new Integer(frsTimeout), requestVO);
		return udpClient.sendMessage();
	}

	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
