
package com.wistron.license.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.common.ActivationResponseVO;
import com.wistron.license.common.DefaultResponseVO;
import com.wistron.license.common.PrepaidInfoVO;
import com.wistron.license.common.PrepaidResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.StatusResponseVO;
import com.wistron.license.common.TokenResponseVO;
import com.wistron.license.jwthandler.TokenAuthenticationService;
import com.wistron.license.service.ActivationService;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.HttpService;

@Service
public class ActivationServiceImpl implements ActivationService {

	private final static Logger LOGGER = LoggerFactory.getLogger(ActivationServiceImpl.class);

	@Autowired
	private HttpService httpService;

	@Autowired
	private EncryptService encryptService;

	@Value("${token.file}")
	private String tokenFilePath;

	@Override
	public DefaultResponseVO activate(ActivationInfoVO requestVO) {
		if (StringUtils.isBlank(requestVO.getCode()) || requestVO.getCode().length() > 36) {
			LOGGER.error("[ActivationServiceImpl][activate] Activation code is invalidated.");
			return new DefaultResponseVO(ResponseMsg.ARGUMENTS_ERROR);
		}

		if (this.encryptService.isActivated()) {
			LOGGER.error("[ActivationServiceImpl][activate] Duplicate activation");
			return new DefaultResponseVO(ResponseMsg.ACTIVATE_DUPLICATE);
		}

		LOGGER.debug("[ActivationServiceImpl][activate] Original requestVO: {}", requestVO.toString());

		String projectId = System.getenv("PROJECT_ID");

		if (StringUtils.isEmpty(projectId)) {
			LOGGER.error("[ActivationServiceImpl][activate] project id is null");
			return new DefaultResponseVO(ResponseMsg.INIT_ERROR);
		}
		requestVO.setProjectID(projectId);
		requestVO.setDockerID(this.encryptService.getUniqueId()); // Do not use docker wording
		LOGGER.debug("[ActivationServiceImpl][activate] Completed requestVO: {}", requestVO.toString());

		try {
			final Gson gson = new Gson();
			ActivationResponseVO responseVO = gson.fromJson(httpService.sendCodeToServer(requestVO),
					ActivationResponseVO.class);
			LOGGER.debug("[ActivationServiceImpl][activate] responseVO: {}", responseVO.toString());

			String snsecret = responseVO.getSnsecret();
			if (StringUtils.isNotEmpty(snsecret)) {
				if (this.encryptService.initModule(requestVO.getCode(), snsecret)) {
					TokenAuthenticationService.createAuthenticationJWT(requestVO.getProjectID(), tokenFilePath);
					return new DefaultResponseVO(ResponseMsg.OK);
				} else {
					LOGGER.error("[ActivationServiceImpl][activate] initModule fail");
					return new DefaultResponseVO(ResponseMsg.INIT_ERROR);
				}
			} else {
				LOGGER.error("[ActivationServiceImpl][activate] On line activate fail.");
				return new DefaultResponseVO(ResponseMsg.lookup(responseVO.getMessage()));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return new DefaultResponseVO(ResponseMsg.SERVER_ERROR);
	}

	@Override
	public TokenResponseVO getToken() {
		final boolean isActivated = this.encryptService.isActivated();
		TokenResponseVO responseVO;
		if (isActivated) {
			String jwt = TokenAuthenticationService.getJWT(tokenFilePath);
			if (StringUtils.isNotEmpty(jwt)) {
				responseVO = new TokenResponseVO(jwt, ResponseMsg.OK.toString());
			} else {
				responseVO = new TokenResponseVO(null, ResponseMsg.SERVER_ERROR.toString());
			}
		} else {
			responseVO = new TokenResponseVO(null, ResponseMsg.NOT_ACTIVATE.toString());
		}
		return responseVO;
	}

	@Override
	public PrepaidResponseVO prepaid(PrepaidInfoVO requestVO) {
		PrepaidResponseVO responseVO = new PrepaidResponseVO();
		if (StringUtils.isBlank(requestVO.getSn()) || requestVO.getSn().length() > 1000) {
			responseVO.setMessage(ResponseMsg.ARGUMENTS_ERROR.toString());
		} else {
			responseVO.setUsageCount(this.encryptService.addPurchase(requestVO.getSn()));
			responseVO.setActivated(this.encryptService.isActivated());
			if (responseVO.getUsageCount() < 0) {
				responseVO.setMessage(ResponseMsg.INVALIDATE_CODE.toString());
			} else {
				responseVO.setMessage(ResponseMsg.OK.toString());
			}
		}
		// TODO Must handle INVALIDATE_SN/ SN_DUPLICATE
		LOGGER.debug("[ActivationServiceImpl][prepaid] responseVO = {}", responseVO.toString());
		return responseVO;
	}

	@Override
	public StatusResponseVO getStatus() {
		boolean isActivated = this.encryptService.isActivated();
		long count = this.encryptService.getUsageCount();
		return new StatusResponseVO(isActivated, count, null, ResponseMsg.OK.toString());
	}

}
