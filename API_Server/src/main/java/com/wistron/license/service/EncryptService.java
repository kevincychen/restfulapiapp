
package com.wistron.license.service;

import com.wistron.license.securityModule.util.ChargeLog;
import com.wistron.license.securityModule.util.PurchaseHistory;
import com.wistron.license.securityModule.util.UsageSign;

public interface EncryptService {
	// ================================================
	// == [Method] Block Start
	// ====

	public String getMigrationInfo() throws Exception;

	public String getUniqueId();

	public boolean isActivated();

	public boolean initModule(String code, String SNSecret);

	public long getUsageCount();

	public long addPurchase(String SerialNumber);

	public PurchaseHistory getPurchaseList();

	public UsageSign getUsageSign(String id, String faceImage);

	public boolean migrate(String migrateCode) throws Exception;

	//spublic boolean charge(String sign);

	public ChargeLog getChargebackHistory(long start, long end);

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
