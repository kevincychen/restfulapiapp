
package com.wistron.license.interceptor;

import java.util.HashSet;
import java.util.Set;

public class FrequencyStruct {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	String uniqueKey;
	long start;
	long end;
	int time;
	int limit;
	Set<String> accessPoints = new HashSet<String>();

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	@Override
	public String toString() {
		return "FrequencyStruct [uniqueKey=" + uniqueKey + ", start=" + start + ", end=" + end + ", time=" + time
				+ ", limit=" + limit + ", accessPoints=" + accessPoints + "]";
	}

	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	public void reset(long timeMillis, String requestId) {

		start = end = timeMillis;
		accessPoints.clear();
		accessPoints.add(requestId);
	}

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
