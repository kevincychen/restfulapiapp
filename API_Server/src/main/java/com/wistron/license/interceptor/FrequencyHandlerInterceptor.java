
package com.wistron.license.interceptor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.google.gson.Gson;
import com.wistron.license.annotation.Frequency;
import com.wistron.license.common.ResponseMsg;

@Component
public class FrequencyHandlerInterceptor extends HandlerInterceptorAdapter {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private Logger LOGGER = LoggerFactory.getLogger(FrequencyHandlerInterceptor.class);
	private static final int MAX_BASE_STATION_SIZE = 100000;
	private static Map<String, FrequencyStruct> BASE_STATION = new HashMap<String, FrequencyStruct>(
			MAX_BASE_STATION_SIZE);
	private static final float SCALE = 0.75F;
	private static final int MAX_CLEANUP_COUNT = 3;
	private static final int CLEANUP_INTERVAL = 1000;
	private Object syncRoot = new Object();
	private int cleanupCount = 0;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		Frequency methodFrequency = ((HandlerMethod) handler).getMethodAnnotation(Frequency.class);
		Frequency classFrequency = ((HandlerMethod) handler).getBean().getClass().getAnnotation(Frequency.class);

		String requestId = UUID.randomUUID().toString();
		request.setAttribute("requestId", requestId);

		boolean going = true;
		if (classFrequency != null) {
			going = handleFrequency(request, response, classFrequency);
		}

		if (going && methodFrequency != null) {
			going = handleFrequency(request, response, methodFrequency);
		}
		return going;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		Frequency methodFrequency = ((HandlerMethod) handler).getMethodAnnotation(Frequency.class);
		if (methodFrequency != null) {
			String key = methodFrequency.name();
			FrequencyStruct frequencyStruct = BASE_STATION.get(key);
			if (frequencyStruct != null) {

				frequencyStruct.accessPoints.remove(request.getAttribute("requestId"));

			}
		}

	}

	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	private boolean handleFrequency(HttpServletRequest request, HttpServletResponse response, Frequency frequency)
			throws IOException {

		boolean going = true;
		if (frequency == null) {
			return going;
		}

		String name = frequency.name();
		final Map<String, Integer> frequencyMap = getFrequencyEnv();
		LOGGER.debug("limit and time {}", new Gson().toJson(frequencyMap));
		int limit = frequencyMap.get("limit");
		int time = frequencyMap.get("time");

		if (time == 0 || limit == 0) {
			going = false;
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return going;
		}

		long currentTimeMilles = System.currentTimeMillis() / 1000;

		String key = name;
		FrequencyStruct frequencyStruct = BASE_STATION.get(key);

		if (frequencyStruct == null) {

			frequencyStruct = new FrequencyStruct();
			frequencyStruct.uniqueKey = name;
			frequencyStruct.start = frequencyStruct.end = currentTimeMilles;
			frequencyStruct.limit = limit;
			frequencyStruct.time = time;
			frequencyStruct.accessPoints.add((String) request.getAttribute("requestId"));

			synchronized (syncRoot) {
				BASE_STATION.put(key, frequencyStruct);
			}
			if (BASE_STATION.size() > MAX_BASE_STATION_SIZE * SCALE) {
				cleanup(currentTimeMilles);
			}
		} else {

			frequencyStruct.end = currentTimeMilles;
			frequencyStruct.accessPoints.add((String) request.getAttribute("requestId"));
		}

		if (frequencyStruct.end - frequencyStruct.start >= time) {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("frequency struct be out of date, struct will be reset., struct: {}",
						frequencyStruct.toString());
			}
			frequencyStruct.reset(currentTimeMilles, (String) request.getAttribute("requestId"));
		} else {
			int count = frequencyStruct.accessPoints.size();
			if (count > limit) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("key: {} too frequency. count: {}, limit: {}.", key, count, limit);
				}
				going = false;
				frequencyStruct.accessPoints.remove(request.getAttribute("requestId"));
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				throw new IOException(ResponseMsg.SERVER_TOO_BUSY.toString());

			}
		}
		return going;
	}

	private void cleanup(long currentTimeMilles) {

		LOGGER.debug("clean ===============");

		synchronized (syncRoot) {

			Iterator<String> it = BASE_STATION.keySet().iterator();
			while (it.hasNext()) {

				String key = it.next();
				FrequencyStruct struct = BASE_STATION.get(key);
				if ((currentTimeMilles - struct.end) > struct.time) {
					it.remove();
				}
			}

			if ((MAX_BASE_STATION_SIZE - BASE_STATION.size()) > CLEANUP_INTERVAL) {
				cleanupCount = 0;
			} else {
				cleanupCount++;
			}

			if (cleanupCount > MAX_CLEANUP_COUNT) {
				randomCleanup(MAX_CLEANUP_COUNT);
			}
		}
	}

	private Map<String, Integer> getFrequencyEnv() {
		final Map<String, Integer> returnMap = new HashMap<>();
		String envFrequency = System.getenv("FREQUENCY");
		LOGGER.debug("frequency = {}", envFrequency);
		int limit = 5;
		int time = 10;
		if (StringUtils.isEmpty(envFrequency)) {
			envFrequency = "10";
		}

		if (!NumberUtils.isDigits(envFrequency)) {
			returnMap.put("limit", limit);
		} else {
			returnMap.put("limit", new Integer(envFrequency));
		}

		returnMap.put("time", time);
		return returnMap;

	}

	private void randomCleanup(int count) {

		if (BASE_STATION.size() < MAX_BASE_STATION_SIZE * SCALE) {
			return;
		}

		Iterator<String> it = BASE_STATION.keySet().iterator();
		Random random = new Random();
		int tempCount = 0;

		while (it.hasNext()) {
			if (random.nextBoolean()) {
				it.remove();
				tempCount++;
				if (tempCount >= count) {
					break;
				}
			}
		}
	}

	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
