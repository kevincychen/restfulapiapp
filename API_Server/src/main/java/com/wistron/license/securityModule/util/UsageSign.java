
package com.wistron.license.securityModule.util;

import java.io.Serializable;

import com.wistron.license.common.BasicVO;

public class UsageSign extends BasicVO implements Serializable {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 930314654285158099L;

	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private int Count = 0;

	private String Error;

	private String Sign;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public int getCount() {
		return this.Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public String getError() {
		return this.Error;
	}

	public void setError(String error) {
		Error = error;
	}

	public String getSign() {
		return this.Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
