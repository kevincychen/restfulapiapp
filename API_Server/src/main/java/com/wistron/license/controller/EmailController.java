package com.wistron.license.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.wistron.license.common.EmailInfoVO;
import com.wistron.license.common.EmailResponseVO;
import com.wistron.license.service.EmailService;

@RestController
@CrossOrigin
@RequestMapping("/notification")
public class EmailController {

	private final static Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

	@Autowired
	private EmailService emailService;

	@RequestMapping(value = "/getEmailInfo", method = RequestMethod.GET)
	public ResponseEntity<?> getEmailInfo() throws IOException {
		LOGGER.info("[ActivationController][getEmailInfo]");
		EmailInfoVO emailInfo = this.emailService.getMailInfo();
		return new ResponseEntity<EmailInfoVO>(emailInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "/testSendEmail", method = RequestMethod.POST)
	public ResponseEntity<?> testSendEmail(@RequestBody EmailInfoVO emailInfoVO) {
		LOGGER.info("[ActivationController][testSendEmail] emailInfoVO: {}", new Gson().toJson(emailInfoVO));
		EmailResponseVO emailResponseVO = this.emailService.testSendEmail(emailInfoVO);
		if (emailResponseVO.isSuccess()) {
			return new ResponseEntity<EmailResponseVO>(emailResponseVO, HttpStatus.OK);
		} else {
			return new ResponseEntity<EmailResponseVO>(emailResponseVO, HttpStatus.FORBIDDEN);
		}
	}

	@RequestMapping(value = "/saveEmailInfo", method = RequestMethod.POST)
	public ResponseEntity<?> saveEmailInfo(@RequestBody EmailInfoVO emailInfoVO) {
		LOGGER.info("[ActivationController][saveEmailInfo]");
		EmailResponseVO emailResponseVO = this.emailService.saveEmailInfo(emailInfoVO);
		if (emailResponseVO.isSuccess()) {
			return new ResponseEntity<EmailResponseVO>(emailResponseVO, HttpStatus.OK);
		} else {
			return new ResponseEntity<EmailResponseVO>(emailResponseVO, HttpStatus.FORBIDDEN);
		}
	}
}
