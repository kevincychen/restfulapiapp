
package com.wistron.license.controller;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wistron.license.common.LogResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.service.LogService;

@RestController
@CrossOrigin
public class FrsLogController {

	@Autowired
	private LogService logService;

	@Value("${temp_path}")
	private String tempPath;

	private void setHttpStatus(String resMsg, HttpServletResponse response) {
		if (ResponseMsg.lookup(resMsg) == ResponseMsg.OK) {
			response.setStatus(org.apache.http.HttpStatus.SC_OK);
		} else {
			response.setStatus(org.apache.http.HttpStatus.SC_BAD_REQUEST);
		}
		// Note: Because ZAP cannot accept 500 error, we use 400 for all error cases.
	}

	@RequestMapping("/log")
	public @ResponseBody LogResponseVO queryLog(@RequestParam int[] actions,
			@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date from,
			@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date end, HttpServletResponse response) {
		LogResponseVO responseVO = this.logService.getLog(actions, from, end);
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping("/log/export")
	public void exportLog(@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date from,
			@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date end, HttpServletResponse response) {
		final String fileName = this.logService.exportLog(from, end);
		boolean isOK = false;
		if (fileName != null && fileName != ResponseMsg.SERVER_ERROR.toString()
				&& fileName != ResponseMsg.ARGUMENTS_ERROR.toString()) {
			InputStream is = null;
			try {
				response.setHeader("Content-disposition", "attachment; filename=" + fileName);
				is = new FileInputStream(tempPath + fileName);
				org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				response.flushBuffer();
				is.close();
				is = null;
				isOK = true;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				IOUtils.closeQuietly(is);
			}
		}
		response.setStatus((isOK) ? org.apache.http.HttpStatus.SC_OK : org.apache.http.HttpStatus.SC_BAD_REQUEST);
	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
