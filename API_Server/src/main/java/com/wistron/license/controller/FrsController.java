
package com.wistron.license.controller;

import static com.wistron.license.common.FrsAction.DELETE;
import static com.wistron.license.common.FrsAction.ENROLL;
import static com.wistron.license.common.FrsAction.RECOGNITION;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wistron.license.annotation.Frequency;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResponseVO;
import com.wistron.license.service.FrsService;

@RestController
@RequestMapping("/frs")
@CrossOrigin
public class FrsController {

	@Autowired
	private FrsService frsService;

	@Frequency
	@PostMapping("/enroll")
	public FrsResponseVO enroll(@RequestBody FrsRequestVO requestVO, HttpServletResponse response) {
		requestVO.setAction(ENROLL.getValue());
		final FrsResponseVO frsResponseVO = this.frsService.enroll(requestVO);
		if (frsResponseVO.getStatus() != 0 && frsResponseVO.getStatus() != -1) {
			if (frsResponseVO.getStatus() >= -5 && frsResponseVO.getStatus() <= -1) {
				response.setStatus(HttpStatus.SC_BAD_REQUEST);
			} else {
				response.setStatus(HttpStatus.SC_FORBIDDEN);
			}
		}
		return frsResponseVO;

	}

	@Frequency
	@PostMapping("/recognition")
	public FrsResponseVO recognition(@RequestBody FrsRequestVO requestVO, HttpServletResponse response) {
		requestVO.setAction(RECOGNITION.getValue());
		final FrsResponseVO frsResponseVO = this.frsService.recognition(requestVO);
		if (frsResponseVO.getStatus() != 0 && frsResponseVO.getStatus() != -1) {
			if (frsResponseVO.getStatus() >= -5 && frsResponseVO.getStatus() <= -1) {
				response.setStatus(HttpStatus.SC_BAD_REQUEST);
			} else {
				response.setStatus(HttpStatus.SC_FORBIDDEN);
			}
		}
		return frsResponseVO;

	}

	@Frequency
	@PostMapping("/delete")
	public FrsResponseVO delete(@RequestBody FrsRequestVO requestVO, HttpServletResponse response) {
		requestVO.setAction(DELETE.getValue());
		final FrsResponseVO frsResponseVO = this.frsService.delete(requestVO);
		if (frsResponseVO.getStatus() != 0 && frsResponseVO.getStatus() != -1) {
			if (frsResponseVO.getStatus() >= -5 && frsResponseVO.getStatus() <= -1) {
				response.setStatus(HttpStatus.SC_BAD_REQUEST);
			} else {
				response.setStatus(HttpStatus.SC_FORBIDDEN);
			}
		}
		return frsResponseVO;

	}

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
