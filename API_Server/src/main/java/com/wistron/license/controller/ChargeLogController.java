package com.wistron.license.controller;

import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wistron.license.common.CountingLogResponseVO;
import com.wistron.license.common.PrepaidLogResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.service.ChargeLogService;

@CrossOrigin
@RestController
@RequestMapping("/chargelog")
public class ChargeLogController {

	@Autowired
	private ChargeLogService chargeLogService;

	private void setHttpStatus(String resMsg, HttpServletResponse response) {
		if (ResponseMsg.lookup(resMsg) == ResponseMsg.OK) {
			response.setStatus(org.apache.http.HttpStatus.SC_OK);
		} else {
			response.setStatus(org.apache.http.HttpStatus.SC_BAD_REQUEST);
		}
		// Note: Because ZAP cannot accept 500 error, we use 400 for all error cases.
	}

	@RequestMapping(value = "/counting", method = RequestMethod.GET)
	public @ResponseBody CountingLogResponseVO queryCountingLog(
			@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date from,
			@RequestParam @DateTimeFormat(pattern = "yyyyMMdd") Date end, HttpServletResponse response) {
		CountingLogResponseVO responseVO = this.chargeLogService.queryCountingLog(from, end);
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping(value = "/prepaid", method = RequestMethod.GET)
	public @ResponseBody PrepaidLogResponseVO queryPrepaidLog(HttpServletResponse response) {
		PrepaidLogResponseVO responseVO = this.chargeLogService.queryPrepaidLog();
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

}
