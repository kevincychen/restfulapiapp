
package com.wistron.license.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wistron.license.common.MigrationRequestVO;
import com.wistron.license.common.MigrationResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.service.MigrationService;

@RestController
@CrossOrigin
@RequestMapping("/migration")
public class MigrationController {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Autowired
	private MigrationService migrationService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	@RequestMapping(value = "/getInfo", method = RequestMethod.GET)
	public @ResponseBody MigrationResponseVO getMigrationInfo(HttpServletResponse response) {
		MigrationResponseVO responseVO = this.migrationService.getMigrationInfo();
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping(value = "/migrate", method = RequestMethod.POST)
	public @ResponseBody MigrationResponseVO migrate(@RequestBody MigrationRequestVO requestVO,
			HttpServletResponse response) {

		MigrationResponseVO responseVO = this.migrationService.migrate(requestVO);
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	// ####################################################################
	// ## [Method] sub-block :

	private void setHttpStatus(String resMsg, HttpServletResponse response) {
		if (ResponseMsg.lookup(resMsg) == ResponseMsg.OK) {
			response.setStatus(org.apache.http.HttpStatus.SC_OK);
		} else {
			response.setStatus(org.apache.http.HttpStatus.SC_BAD_REQUEST);
		}
		// Note: Because ZAP cannot accept 500 error, we use 400 for all error cases.
	}

	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
