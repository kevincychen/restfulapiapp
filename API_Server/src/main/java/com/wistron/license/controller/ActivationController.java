package com.wistron.license.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.common.DefaultResponseVO;
import com.wistron.license.common.PrepaidInfoVO;
import com.wistron.license.common.PrepaidResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.common.StatusResponseVO;
import com.wistron.license.common.TokenResponseVO;
import com.wistron.license.service.ActivationService;

@RestController
public class ActivationController {
 
	@Autowired
	private ActivationService activationService;

	private void setHttpStatus(String resMsg, HttpServletResponse response) {
		if (ResponseMsg.lookup(resMsg) == ResponseMsg.OK) {
			response.setStatus(org.apache.http.HttpStatus.SC_OK);
		} else {
			response.setStatus(org.apache.http.HttpStatus.SC_BAD_REQUEST);
		}
		// Note: Because ZAP cannot accept 500 error, we use 400 for all error cases.
	}

	@RequestMapping(value = "/activate", method = RequestMethod.POST)  
	public DefaultResponseVO activate(@RequestBody ActivationInfoVO requestVO, HttpServletResponse response) {
		DefaultResponseVO responseVO = activationService.activate(requestVO);
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping(value = "/token", method = RequestMethod.GET)
	public TokenResponseVO getToken(HttpServletResponse response) {
		TokenResponseVO responseVO = activationService.getToken();
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public StatusResponseVO getStatus(HttpServletResponse response) {
		StatusResponseVO responseVO = this.activationService.getStatus();
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}

	@RequestMapping(value = "/prepaid", method = RequestMethod.POST)
	public PrepaidResponseVO prepaid(@RequestBody PrepaidInfoVO requestVO, HttpServletResponse response) {
		PrepaidResponseVO responseVO = this.activationService.prepaid(requestVO);
		this.setHttpStatus(responseVO.getMessage(), response);
		return responseVO;
	}
}
