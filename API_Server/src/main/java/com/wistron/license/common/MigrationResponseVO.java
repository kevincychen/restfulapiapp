
package com.wistron.license.common;

import java.io.Serializable;

public class MigrationResponseVO extends BasicVO implements Serializable {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 5069245525440858289L;
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private String migrateCode;

	private String projectId;

	private Boolean migrateResult;

	private String message;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	public MigrationResponseVO() {

	}

	public MigrationResponseVO(String migrateCode, Boolean migrateResult) {
		this.migrateCode = migrateCode;
		this.migrateResult = migrateResult;
	}

	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public String getMigrateCode() {
		return this.migrateCode;
	}

	public void setMigrateCode(String migrateCode) {
		this.migrateCode = migrateCode;
	}

	public Boolean isMigrateResult() {
		return this.migrateResult;
	}

	public void setMigrateResult(Boolean migrateResult) {
		this.migrateResult = migrateResult;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProjectId() {
		return this.projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
