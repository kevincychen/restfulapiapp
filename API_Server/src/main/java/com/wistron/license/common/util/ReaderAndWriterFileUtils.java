package com.wistron.license.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wistron.license.common.EmailResponseVO;

public class ReaderAndWriterFileUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(ReaderAndWriterFileUtils.class);

	public static String readerFile(String filePath) throws IOException {

		Path path = Paths.get(filePath);
		if (!Files.exists(path)) {
			FileUtils.writeStringToFile(new File(filePath), "", "UTF-8");
			LOGGER.info("*** *** create New File {}.", filePath);
		}

		String fileString = null;
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(filePath));
			StringBuffer stringBuffer = new StringBuffer();
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
			}
			bufferedReader.close();
			fileString = stringBuffer.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileString;
	}

	public static EmailResponseVO writerFile(String fileStr, String filePath) {
		LOGGER.info("[EmailServiceImpl][writerFile]");
		EmailResponseVO emailResponseVO = new EmailResponseVO();
		PrintWriter printWriter = null;
		try {
			FileUtils.writeStringToFile(new File(filePath), "", "UTF-8");

			Writer fileWriter = new FileWriter(filePath, false);
			printWriter = new PrintWriter(fileWriter);
			printWriter.write(fileStr);
		} catch (FileNotFoundException e) {
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("IOException");
			e.printStackTrace();
		} finally {
			if (printWriter != null) {
				printWriter.close();
			}
		}
		return emailResponseVO;
	}

}
