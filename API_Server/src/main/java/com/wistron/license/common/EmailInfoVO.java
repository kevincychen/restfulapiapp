package com.wistron.license.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailInfoVO implements Serializable {
	
	private static final long serialVersionUID = -1659477336028116414L;
	
	private String host;
    private Integer port;
    private String senderEmail;
    private String senderPassword;
    private String recipients;
    
    public EmailInfoVO() {
    		this.host = "";
		this.port = 25;
		this.senderEmail = "";
		this.senderPassword = "";
		this.recipients = "";
	}

	public EmailInfoVO(String host, Integer port, String senderEmail, 
			String senderPassword, String recipients) {
		super();
		this.host = host;
		this.port = port;
		this.senderEmail = senderEmail;
		this.senderPassword = senderPassword;
		this.recipients = recipients;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderPassword() {
		return senderPassword;
	}

	public void setSenderPassword(String senderPassword) {
		this.senderPassword = senderPassword;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	
}
