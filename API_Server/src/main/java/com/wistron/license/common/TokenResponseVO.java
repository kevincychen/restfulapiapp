package com.wistron.license.common;

public class TokenResponseVO extends BasicVO {
	
	private static final long serialVersionUID = 8612613338591121166L;
			
	private String token;
	private String message;

	public TokenResponseVO() {
	}

	public TokenResponseVO(String token, String message) {
		this.token = token;
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
