
package com.wistron.license.common.util;

import com.wistron.license.common.BasicVO;

public class FileVO extends BasicVO {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 4594013540893320822L;
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private String sign;

	private String faceImage;

	private String action;

	private String userId;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	public FileVO(String sign, String faceImage, String action, String userId) {
		this.sign = sign;
		this.faceImage = faceImage;
		this.action = action;
		this.userId = userId;
	}

	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public String getSign() {
		return this.sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getFaceImage() {
		return this.faceImage;
	}

	public void setFaceImage(String faceImage) {
		this.faceImage = faceImage;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
