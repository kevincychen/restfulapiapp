package com.wistron.license.common;

public class ActivationResponseVO extends BasicVO {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 651690444948248330L;
	private String snsecret;

	private String message;

	public String getSnsecret() {
		return snsecret;
	}

	public void setSnsecret(String snsecret) {
		this.snsecret = snsecret;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
