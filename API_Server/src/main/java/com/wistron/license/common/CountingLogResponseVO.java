package com.wistron.license.common;

import java.util.List;

public class CountingLogResponseVO extends BasicVO {
	private static final long serialVersionUID = 6291720786975270169L;
	private boolean overcount = false;
	private List<String> logs;
	private String message;

	public CountingLogResponseVO(boolean overcount, List<String> logs, ResponseMsg message) {

		this.overcount = overcount;
		this.logs = logs;
		if (message != null) {
			this.message = message.getValue();
		}
	}

	public boolean isOvercount() {
		return overcount;
	}

	public void setOvercount(boolean overcount) {
		this.overcount = overcount;
	}

	public List<String> getLogs() {
		return logs;
	}

	public void setLogs(List<String> logs) {
		this.logs = logs;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
