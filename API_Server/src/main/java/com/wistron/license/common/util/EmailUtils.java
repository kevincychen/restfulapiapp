package com.wistron.license.common.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wistron.license.common.EmailContextVO;
import com.wistron.license.common.EmailInfoVO;
import com.wistron.license.common.EmailResponseVO;

public class EmailUtils {

	private final static Logger LOGGER = LoggerFactory.getLogger(EmailUtils.class);
	
	public final static EmailResponseVO sendMail(EmailContextVO emailContextVO, EmailInfoVO emailInfoVO) {
		LOGGER.info("[EmailUtils][sendMail]");
		
		EmailResponseVO emailResponseVO = new EmailResponseVO();
		if(!checkEmailData(emailContextVO, emailInfoVO)) {
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("INFOS_INCOMPLETE");
			return emailResponseVO;
		}
		
		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.host", emailInfoVO.getHost());
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", emailInfoVO.getPort());
		
		LOGGER.info("[EmailUtils][mailSession]");
		Session mailSession = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailInfoVO.getSenderEmail(), emailInfoVO.getSenderPassword());
			}
		});
		
		Transport transport = null;
		MimeMessage message = new MimeMessage(mailSession);
		try {
			transport = mailSession.getTransport();
			
			message.setSubject(emailContextVO.getSubject());
			message.setContent(emailContextVO.getContext(), "text/plain");
			new InternetAddress();
			String recipientsStr = emailInfoVO.getRecipients().toString();
			LOGGER.info("[EmailUtils][message] Recipients = {}", recipientsStr );
			
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientsStr));
			transport.connect();
			transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
			transport.close();
			
		} catch (NoSuchProviderException e) {
			LOGGER.error("[EmailUtils][NoSuchProviderException] = {}", e.getMessage());
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("NoSuchProviderException");
		} catch (AddressException e) {
			LOGGER.error("[EmailUtils][AddressException] = {}", e.getMessage());
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("AddressException");
		} catch (MessagingException e) {
			LOGGER.error("[EmailUtils][MessagingException] = {}", e.getMessage());
			emailResponseVO.setSuccess(false);
			emailResponseVO.setErrorMessage("MessagingException");
		}
		return emailResponseVO;
	}
	
	private static boolean checkEmailData(final EmailContextVO emailContextVO, final EmailInfoVO emailInfoVO) {
		boolean isValid = true;
		if(emailContextVO == null || emailInfoVO == null) {
			LOGGER.warn("[EmailUtils][checkEmailData] send content or email infos can not null.");
			isValid = false;
		}else if(StringUtils.isBlank(emailContextVO.getSubject()) || 
				StringUtils.isBlank(emailContextVO.getContext()) ) {
			LOGGER.warn("[EmailUtils][checkEmailData] subject or context are not blank.");
			isValid = false;
		}else if(StringUtils.isBlank(emailInfoVO.getHost()) || 
				StringUtils.isBlank(emailInfoVO.getSenderEmail()) || 
				StringUtils.isBlank(emailInfoVO.getSenderPassword()) || 
				StringUtils.isBlank(String.valueOf(emailInfoVO.getPort())) || 
				emailInfoVO.getRecipients() == null ) {
			LOGGER.warn("[EmailUtils][checkEmailData] email infos are incomplete.");
			isValid = false;
		}
		return isValid;
	}
	
}
