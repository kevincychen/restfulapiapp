package com.wistron.license.common;

public class EmailResponseVO extends BasicVO {

	private static final long serialVersionUID = 1399927154296177658L;
	
	private boolean isSuccess = true;

	private String errorMessage;

	public EmailResponseVO() {
		this.errorMessage = "";
	}
	
	public EmailResponseVO(String errorMessage) {
		this.isSuccess = false;
		this.errorMessage = errorMessage;
	}
	
	public EmailResponseVO(boolean isSuccess, String errorMessage) {
		this.isSuccess = isSuccess;
		this.errorMessage = errorMessage;
	}
	
	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	

}
