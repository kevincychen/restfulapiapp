
package com.wistron.license.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	public final static Date getDateStart(final Date date) {
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime();
		} else
			return null;
	}

	public final static Date getDateEnd(final Date date) {
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 0);
			return cal.getTime();
		} else
			return null;
	}

	public final static String getTodayStr(final String format) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(cal.getTime());
	}

	public final static String getDateStr(final Date date, final String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date.getTime());
	}

	public final static String getDateStr(final Timestamp timestamp, final String format) {
		Date date = new Date(timestamp.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date.getTime());
	}

	public final static String getDateStr(final long time, final String format) {
		Date date = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date.getTime());
	}

	public final static String addDay(final String date, final String format, final int val) throws ParseException {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		cal.setTime(sdf.parse(date));
		cal.add(Calendar.DATE, val);
		return sdf.format(cal.getTime());
	}
	// public final static String get
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
