package com.wistron.license.common;

import com.wistron.license.common.util.DateUtils;
import com.wistron.license.securityModule.util.Purchase;

public class PrepaidLogVO extends BasicVO {

	private static final long serialVersionUID = 6291720786975270169L;

	private String time;
	private String sn;
	private int count;
	private String expireTime;

	public PrepaidLogVO(Purchase purchase) {
		this.time = DateUtils.getDateStr(purchase.getTimestamp(), "yyyy-MM-dd HH:mm:ss.SSS");
		this.expireTime = DateUtils.getDateStr(purchase.getExpireDate(), "yyyy-MM-dd HH:mm:ss.SSS");
		this.sn = purchase.getSn();
		this.count = purchase.getCount();

	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

}
