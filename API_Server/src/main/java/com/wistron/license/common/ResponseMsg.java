package com.wistron.license.common;

import org.apache.commons.lang3.StringUtils;

public enum ResponseMsg {
	// for success
	OK("OK"), //
	// for activate
	INVALIDATE_CODE("INVALIDATE_CODE"), //
	INVALID_ENDTIME("INVALID_ENDTIME"), //
	INVALID_PROJECT_ID_OR_CODE("INVALID_PROJECT_ID_OR_CODE"), //
	INVALID_PROJECT_ID("INVALID_PROJECT_ID"), //
	ARGUMENTS_ERROR("ARGUMENTS_ERROR"), //
	ACTIVATE_DUPLICATE("ACTIVATE_DUPLICATE"), //
	INIT_ERROR("INIT_ERROR"), //
	// for get token
	NOT_ACTIVATE("NOT_ACTIVATE"), //
	// for prepaid
	SN_NOT_FOUND("SN_NOT_FOUND"), //
	INVALIDATE_SN("INVALIDATE_SN"), //
	SN_DUPLICATE("SN_DUPLICATE"), //
	// for general
	SERVER_ERROR("	SERVER_ERROR"), //
	FRS_TIMEOUT("FRS_TIMEOUT"), //
	SERVER_TOO_BUSY("SERVER_TOO_BUSY"),//
	;

	private String value;

	public String getValue() {
		return this.value;
	}

	ResponseMsg(String value) {
		this.value = value;
	}

	public static final ResponseMsg lookup(String value) {
		final ResponseMsg[] msgs = ResponseMsg.values();
		ResponseMsg returnMsg = null;
		for (ResponseMsg responseMsg : msgs) {
			if (StringUtils.equals(value, responseMsg.getValue())) {
				returnMsg = responseMsg;
				break;
			}
		}
		return returnMsg;
	}
}
