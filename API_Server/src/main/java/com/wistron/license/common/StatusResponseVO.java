package com.wistron.license.common;

public class StatusResponseVO extends BasicVO {

	private static final long serialVersionUID = -7474157512522909518L;
	
	private boolean activateStatus;
	private Long count;
	private String expireTime;
	private String message;
	
	public StatusResponseVO() {
	}

	public StatusResponseVO(boolean activateStatus, Long count, String expireTime, String message) {
		super();
		this.activateStatus = activateStatus;
		this.count = count;
		this.expireTime = expireTime;
		this.message = message;
	}
	
	public boolean isActivateStatus() {
		return activateStatus;
	}
	public void setActivateStatus(boolean activateStatus) {
		this.activateStatus = activateStatus;
	}
	
	public Long getCount() {
		return count;
	}
	
	public void setCount(Long count) {
		this.count = count;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
