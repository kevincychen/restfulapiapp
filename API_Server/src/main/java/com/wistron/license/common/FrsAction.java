
package com.wistron.license.common;

public enum FrsAction {
	ENROLL(0, "add"), RECOGNITION(1, "recognition"), DELETE(2, "delete");

	private int code;

	private String value;

	private FrsAction(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public int getCode() {
		return this.code;
	}

	public String getValue() {
		return this.value;
	}

	public final static FrsAction lookUp(final int code) {
		FrsAction returnAction = null;
		FrsAction[] frsActions = FrsAction.values();
		for (FrsAction frsAction : frsActions) {
			if (code == frsAction.getCode()) {
				returnAction = frsAction;
				break;
			}
		}
		return returnAction;
	}

}
