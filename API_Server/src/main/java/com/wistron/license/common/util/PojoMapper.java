package com.wistron.license.common.util;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PojoMapper {

    private static ObjectMapper obMapper = new ObjectMapper();

    public static <T> Object fromJson(String jsonAsString, Class<T> pojoClass) {
    	try {
    		return obMapper.readValue(jsonAsString, pojoClass);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }

    public static String toJson(Object pojo) {
    	try {
    		return obMapper.writeValueAsString(pojo);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
        return null;
    }
}