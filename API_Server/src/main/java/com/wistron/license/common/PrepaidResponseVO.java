
package com.wistron.license.common;

import java.io.Serializable;

public class PrepaidResponseVO extends BasicVO implements Serializable {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = -6197870597549295399L;
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private boolean isActivated;
	private long usageCount;

	private String message;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public boolean isActivated() {
		return this.isActivated;
	}

	public void setActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}

	public long getUsageCount() {
		return this.usageCount;
	}

	public void setUsageCount(long usageCount) {
		this.usageCount = usageCount;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
