package com.wistron.license.common;

import java.util.List;

public class PrepaidLogResponseVO extends BasicVO {
	private static final long serialVersionUID = 6291720786975270169L;
	private boolean overcount = false;
	private List<PrepaidLogVO> logs;
	private String message;

	public PrepaidLogResponseVO() {
		// Do nothing.
	}

	public PrepaidLogResponseVO(boolean overcount, List<PrepaidLogVO> logs, String message) {

		this.overcount = overcount;
		this.logs = logs;
		this.message = message;
	}

	public boolean isOvercount() {
		return overcount;
	}

	public void setOvercount(boolean overcount) {
		this.overcount = overcount;
	}

	public List<PrepaidLogVO> getLogs() {
		return logs;
	}

	public void setLogs(List<PrepaidLogVO> logs) {
		this.logs = logs;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
