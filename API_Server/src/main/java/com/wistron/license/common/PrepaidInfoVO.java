package com.wistron.license.common;

public class PrepaidInfoVO extends BasicVO {
	
	private static final long serialVersionUID = 6291720786975270169L;
	
	private String sn;

	public PrepaidInfoVO() {
	}

	public PrepaidInfoVO(String sn) {
		this.sn = sn;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}
}
