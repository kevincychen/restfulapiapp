
package com.wistron.license.common;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.wistron.license.logger.util.LogVO;

/**
 *
 * Page/Class Name: LogResponseVO Title: Description: Copyright: Copyright(c)
 * 2004 Wistron Corportion. All Rights Reserved. I Company: Wistron Corportion
 * author: louis Create Date: 2018年1月11日 Last Modifier: louis Last Modify Date:
 * 2018年1月11日 Version 1.0
 *
 */
public class LogResponseVO implements Serializable {

	/**
	 * <code>serialVersionUID</code> 的註解
	 */
	private static final long serialVersionUID = 6291720786975270169L;
	
	private boolean overcount = false;
	private List<LogVO> logs;
	private String message;

	public LogResponseVO(boolean overcount, final List<LogVO> logs, final String message) {
		if (StringUtils.isNotBlank(message)) {
			this.message = message;
		}
		this.logs = logs;
		this.overcount = overcount;
	}
	
	public LogResponseVO() {
	}

	public boolean isOvercount() {
		return overcount;
	}

	public void setOvercount(boolean overcount) {
		this.overcount = overcount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<LogVO> getLogs() {
		return this.logs;
	}

	public void setLogs(List<LogVO> logs) {
		this.logs = logs;
	}

}
