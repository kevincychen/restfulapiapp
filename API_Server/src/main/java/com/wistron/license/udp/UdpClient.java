package com.wistron.license.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.wistron.license.common.FrsRequestVO;

public class UdpClient {

	private final static Logger LOGGER = LoggerFactory.getLogger(UdpClient.class);

	DatagramSocket Socket;
	private int timeout = 5000;
	private int serverPort = 55688;
	private String ipOfFRServer = "localhost";

	private String message;

	public final void setInfo(String ip, int port, int timeout, FrsRequestVO requestVO) {

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		requestVO.setTimestamp(timestamp.getTime());
		final Gson gson = new Gson();
		this.message = gson.toJson(requestVO);
		this.timeout = timeout;
		this.ipOfFRServer = ip;
		this.serverPort = port;

	}

	/**
	 * @param message
	 *            : the content send to Face ID server
	 * @return result from Face ID server
	 * @throws IOException
	 */
	public String sendMessage() throws IOException {
		String ret = "";
		try {
			Socket = new DatagramSocket();
			InetAddress IPAddress = InetAddress.getByName(ipOfFRServer);
			byte[] incomingData = new byte[1024];
			byte[] data = this.message.getBytes();

			DatagramPacket sendPacket = new DatagramPacket(data, data.length, IPAddress, serverPort);
			Socket.send(sendPacket);
			LOGGER.debug("Message sent to Server: {}", this.message);
			LOGGER.debug("listen response at {}:{}", Socket.getLocalAddress().getHostAddress(), Socket.getLocalPort());

			DatagramPacket incomingPacket = new DatagramPacket(incomingData, incomingData.length);
			Socket.setSoTimeout(timeout);
			Socket.receive(incomingPacket);
			ret = new String(incomingPacket.getData());
			LOGGER.debug("[UdpClient][sendMessage]Response from server: {}", ret);
		} catch (UnknownHostException e) {

			throw e;
		} catch (SocketException e) {

			throw e;
		} catch (IOException e) {

			throw e;
		}

		return StringUtils.trimToEmpty(ret);
	}

	@Override
	public String toString() {
		ReflectionToStringBuilder.setDefaultStyle(ToStringStyle.MULTI_LINE_STYLE);
		return ReflectionToStringBuilder.toString(this);
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	// public static void main(String[] args) {
	// UdpClient client = new UdpClient();
	// String message = "";
	// message = "{\"timestamp\":" + "\"" + (new Date()).toString() + "\","
	// +
	// "\"pic\":\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBUQDxAQDxAQDw8QEBAPEA8PDxAPFRUWFhURFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFxAQFy0dFx0rLS0tKy0rLSsrKy0rLSstKy0tLS0tLS0tLS0tLS0tKy0tLSstKzItKy0rLSs3LSsrK//AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xAA9EAACAQIDBQYDBgUCBwAAAAAAAQIDEQQSIQUGMUFRImFxgZGhE7HBByMyUtHwFEJicuEzghUWNENjkqL/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQMCBAX/xAAiEQEBAAICAgIDAQEAAAAAAAAAAQIRAxIhMRNBBDJRIhT/2gAMAwEAAhEDEQA/APTUgkgkh0js288yQ6QSQ6QbGg2HsEkPYBoNh7BWHsGzDYewVh7BsaBYVg7D2FsaBYVg7CsGxoFhWHqTjFOUmoxSu3JpJLq2crtbf/B0NIZ8Q/8AxJKH/s+PkK5Se2scMsvUdTYVjzir9qL/AJcIkv66r+iAp/alK+uHh5SqfoZ+TFT4M/49KsKxx2zftEw1R2qU5QfNxaml5aM6jZ+06GIV6NSM+qTtJeMXqhzkl9VjLizx9xYsKwdhWNbYR2FYOw1h7ALDWJLA2HsAsM0HYZoNkBoFokaBaDYA0C0SNAtC2NI2gWiRoFoBpHYQdhADSxkURT2hFczz+e3aj6EM9pVZfzWI91/ir0L/AInHqSU9op8zzdYqf5mWKGPqR53M/JT+J6ZSxKZYjJM4PBbbtpLQ6HBbUT5m5ntO4WN6w9irRxSZZjNM1tk9hDCsMCQ9gUg4ioKxR2xtSlhaTrVpWiuCX4pS6ItYqvGnBzm7Rirv6Jd97ep4jv5tmtVxElU0yvswu8sIr8MbdebfNmbdN4YdrpLvBvTVxknKby0YvsUovsp9ZfmZzmPqtavV9XyK9Os+z0XEt/A+JCyV3wT7jlzy87r0+PGa1GPObfG79Eizh1F6SsvP/BJPZc7LwM+phnH8XLjoLcp9bG08G0rxba8L+jJ8Bi6lOSlCbTT01fs+TM3Y2NUZZb6PitPkbuIwyl24Kz4yj1XVE8srjVcZMo7rdvfiWkMSnNK15Jfew75L+Zd61O9oVYzipwalGSTjJapo8GUJJpxdpcYtep1e6G8rovtf6bdq1PlBv/uwXTqi3Hz/AFfTl5/xJ+2L1Gw1h4TUkpRd1JJprg0+DHsde3naA0NYMaw9loFgWg2RyqINgmM0RTxCRXqY5LmLseltoFoovaMeoL2iuodoNLzQzRTW0F1H/jYh2LSzYRX/AIqIg7DTyuKJYoFD3OZ3DCRGmGhEkJ6GKnDgyugkGxpvYHbjWktDewu10+ZwqDhUlF3TNTLSeXHK9Lo41PmXIVUzznCbYlHRnQYHbCfMrM0bhY6lMJGbh8cnzL1OpdGt7ZVNvSSoylKWWNNOo3/arr9+B8/bTqurVlOTbcpN3lq/A9X+0zazjSWGg0viNSqPnlXBLz+R5U5JuyJZ108OP2HD4XN/jmbuz9nNa8B9nYbRG7haaOfJ3YVXWCTWq8yritjKUXZXv4I6SnTRZp0UQtsWleSbV2LUpO6T7k+hqbu4x1F8Oek1+Bt//L7ju9rbNjUi9NTz2phnQxC5KUrebej9TXbvBJq7jaVJNNapxbklzVvxR8nr6kc4uElUiu6S5PqvA0MVpKFVcJLtLlnWkvVP2I69NJSh01V+j1Xt8iMvlV225G2Fl/h5O8Ws1Bv8vOHlyOsdZHjmy8RKNsrtKEs0e5rijq/+ZtE3e9k3Y7uHl3NX6eV+Vway3PVdu66IKuLSOMlvMrcX7lDFbxyf4S15HLMK7PEbTiuZkYrb0VzOOr7TqT4uxVbvxMXOqTi/rosXvJ01Mqtt2o+HzZmVGQ5hbtbmEi9PbFXqA9s1epRkRsbXWNSO3Kq5r3J4bw1P22YYhWl1n8dAt46nQYwMwhdqOkao6QyZJEVrR4xDsMg0Z2RrBoSCQ9mSFcZgJjhJLBQuuDaAQSY9k0MLtKcOOptUN4bQtez5cTl8xFi62WDfOw5azcZWDvftWdSrK8s0nppeyXQxtlu8kuV/VkG1Kt5Pmwtl1LVIrv18RWujGajt8GrI1MOjGo1UtW7LvNDDbRpfnXqTzVwblCJahEpYXFwfCSfmX4TTOerQ1SN0cBvlQaea3B39zv5VElqcfvdVhKL1XB80PD2aKVbNh0+9TXmk/qxTrXp05/0qMvWy/feUsBVvQjzSyp+jj+gcf+nt0bXyaMa8t78Gw9XLUfc1L1Wv1L9V8ly+TMSdTVPhdL01X1L9Kvdx74280rr5Mrh4qXLN4pGJoNoYvtwomJMeTImwAajIUSyBsPYRSGuHNAJC7HozBaCYkLZ6RWES2EHYaakaTDVNl/4A8aBTpU+0U4U2TRpMtQok0aIulPtFFUWSxoF2FEsQooXSjtGRPDsaOGNl0A44dD60u0Yjw4Hw2b0sMiKWDDrR2jEcGY+3MRlWRcXxOtrYZRi5PkmzzfeHGNTuuL0j3LqKzTeHlkYmVm5Sf9q+oexW5Vo+bM/E1L8XfibG7lP7xPuEq6+iortTs/HgkQ4vamCfZlZPgmrJ389R9oYCVSNoycU+OXiZ2G3fjdKVNu3Fq6b1vq76jsn21LfpdwuJjT1pzzR90dpsapKpG6OVrYClGKUYONoZFFNWa5Nq3HvOl3XThDKRzk67is3vSnt7G5LxvbTV8LHCYzH4R3TqScr9J2v4nebyYFTi9G25XbTSuungeebZ2WnJyytN+NvQOKY2eT5O0nhsbKkvgPLwsmvKVyynalPulF+WqKe70PunH+lfNr6kqqfdzXWkpL/bLUlZ/qqT9Wfjatkn/TL1TTLNGv2kuii/d/qZW0p3ivP3S/QFYm0l3WXitP1KyJZV29Klmin+/wB6Azw7L2w0p0YvmtGXKmHKTbkutsCVBkMqZu1sOZ+Jos1IXhntETuXXSYLoMfUbVHF2Aymh8Bihh+4z1NnTgwVBmo8MEsKKw2Uosc1P4ZdBC0emqPcHIxZGd1ycGkikHGRCoMONNi7BPGZNGZBGmyeFNi2EsZEkZAQpMnjSHtkkw0go0iVUgDn95q2WllXGTt5fu55dtxN1rcox/fzPSN575/BX9rHnmPjmqTfWUYrySObkvl38M/y5/Hxt6I3dgQkpxktYtJeGnMwtpzvNmnsHa0YKNKad3NKLVrO70uJSe3pmBaa1NNU1bgYeEmbVKfZJ5rYemZj7R19Ea2xKbtyV+Nzn9pV5KWivo7dzFsbE1krSabctHFNLuVr8Qy/UT9nWYind5X00f0OR3hwLjfpY3tnQxLlJVXGUL3i1Fxa7tW7ke2qWam78UQwuqv7cVsSajLK+cWvfX5olxUMs7cnmT8J9l+9n5mdWn8Oeb8rV/Buz+ho4yopx6tWafVW4+at7Dymstj6053GPs68Y6Pxi7FZXlOCX8zil3t2SLuNj25L86uvF6P3sUaEsuV8HCS9UyuLnyejblYnsum7pp2afJ/tM6iVI4DY2NTr/Fg/9SKqSinopJ9r3u/M9J4q/XU6uKbjz/yLZltn1KJWqYS5qyRHKJXrEO9ZLwYEsMasokTiPQ71mvDjKiaDiC4i6w/kqi6IlTLjgM4B0h/LVPIItZBB8cP5ak+GP8IlEPSW0apkkaY6DiGhs8IE8IgRJIsNFtLFEsURRZLFmtDaWKDSI0w0xE5HfCLUrpXajJ+V9fZ+x5xUeifXPJ92trnr+8WC+JFSTtKN/Bp8U+48k2pQcM0Ho0rW7u1w9Dm5cfLv/HymtOVxLvLzBhO0oy/LKL9HceotRox1MxSvWcJO6TvxV/U2cLPQ4rdPH/Eo5Jfjpdm/WPJ/TyOqwtZNOL0urGM1cakxDpJ9posYath7JKVmmnqjBrbIg53cqrXfN6ehZp7Kw+n3lVPmszMZenRx4Suxo46lNdmSenDmZu1bOLsZ8Ni0XZwlVT/Nnl+oWIoqhF2lOStqpyctfEhqbbsk9OA21pNr8ymvZ29yphNoPJBt8ssr+if09Ogt4cWlVT/qXzM+nHjDrFrzWty+vCNy/wBNLaDusy4xfnbg16r2Kbs5JvhNJvx6kuBqZ4+Ti/FJW+nuKlD1WoTwV8tfcumlilTm9JU6yi+TbV17JnrNL8K/tXyPK9z8FOriYSd0qd5NX4rgtPM9U5HZw+nm/lfsaRHIKTAkyzlBIjkg5MjbABaAYTYLYGFjMdsFsYMIa4gCe44Fx7i0Q0EmRphJgE0WSRZBFkkWATxZJFkEWSJjCxFkkWV4yDUhBJJJqzV01Zo4ne3dN1L1KOvWHPg/XidomMxZYytYZ3G7jwiO71SU3mjJW1/C9esfEtz3WqL7xU5qmvxadtR6ntCpxvfKrvjohVKUWmmuKsY+Jb/ov8eP7v4KdGVRtNRlllB8mteB0FKuW9s4KNGeWOl7y9X0MuxDLHTr48tzbdwdVPibVChCXT2OLp13EvUttOKOfOOnB2E6cYR0schvVtWNODu0tLIq7T3tahaKbly5LxOC2ri6laWao7vkuSXgYx493dayzknj2oYqu6tS74X0Ro0tZp91/YzqEO11f1NCSyxduOXKvF8y2XrSOE87HsvjpzbfvoXVaMm3xT06cSLBUskbvjb0QeDpyqSsk7t6c/Ax7qs8e3SbgZv4luV22pX7uiPRmzm90djOhHPNWnK2nRftnRNnocU1i8j8jKXPwaTI5MeTI2yiJpMBsdsjbECYLYmwGxg7YLYzYLYGe4gbiAJbjpkWYdSGSZMJMhUglIQTxYcZECYcZAFiMiRSK8ZEikATqQakQKQSkBLCkFcr57cdDKxm9eCotxniIZlxjC82n00A5G40NmOHxv2i0s2XD0ZVeSlOWRNvgrWuG9tV6sbTajfioKy8BW6bnHafb2JVStJrgrRXkZdiaerGUDlyrvwmppDNENTgXJRK1WBKxeeHPYq7b0bfVcTMrYd36eOrOmrYS5F/weUtY/JIxfDc1XP0sOo683y6In+FfV6JG3h9jSk0rZFfVy1foX8PuRVrT+8rKNJcoq8pfoOY5ZUs88MI5xVE3ZSTb0SWru+CSPSd2N34YeClPtVZpOTa/Dzyog2PufhsPP4iTnJO6z20fU6PMdfFwzHzXn8/5Peax9CuC5AOQDkXcgpSAlIFyAcgB2wJSGlIjcgAmwWwXIFyGYmwWwXIFyADuMR5hAaS46ZFmHzDJKmGpEKY6YBOpEkZFdSCUgJZUg3USV20kuLbsjC2xt6nhtJXlUauoLTzb5HCbX3grYh9qTUeUIu0F5c/MTeOFr0HH72YWjwn8WXSnqr/AN3A5zGfaBVelKlCHRybm/TRHGOTfH5jMW1ZxSL209uYnEP72tOS/KpZIL/auJl3JbA5BbUkkNRqZZKS5NP0O/wNWNWnGceDXo+aOCym5uvtD4c/hzdqc3o3wjPr4PgYy9NY+3VKAagXfgBxw5y2ryaZ8qTB+Aa0cKP/AApjammXSwd2auGwiXInpYdFunCxPK7bkVlgl01LdKgSpEsEOZVm4qeIw83+Co4PwjJejMbHT2lSV6cKGJS5dqjP6ps6bIFGmVnNnPtK/j4X6cDS35jF5MVh6lCfCz4X80amH3lw03bPkbtpNW9+BPvPsqlWbU4qXZ6a+p5pVpfCbp8VCbUb8bdLnVxcvZzcn48j1aNZNXTTXVNNAuR5ngto1aMk6c5LuveL7mmdtsna0a8ek0u1G/uu4u5ssLGm5EbkC5AuQMicgXIByBcgA3IFyAbBcgMeYchzDDNPmHuQZglICTqQ6kQ5h8wEnUgMXi40qcqkuEVfx6IBSOc32xmWnCkv525PwWi9wOTdcztDGSrVJTk9ZO9uVunkVEgU9Q2yVrqk1CsIUfEWYGiGdhxWAjWCiNYdIQd9udthVY/Aqv7yK7DfGcenijqFQPIKNRwalBtSi001o01wsembr7ejiY5Z2VaK7UeUl+aPcc3LhZ5jp489+K2I0wXDUtWGUDmq6OMSaEB40yeMRGGNMlyiSCsDJ8oISYhhibZmo5pPhGNzyXEVc83LrJs777QdpfDi6afaqWXlzPOU9G/JHZwY6m3Ny36PKprxXcXMFipQkmnZp8jOpay7krkinqdEqNj0bAY1VaamuPPxJnI5rdjF8YP9s6ByKObKao3IFyAcgXIGRuQLkA5AuQGPMIizDgaS4UZCEAFce4wgISZwe9WKdTESXKmlBeWr9xxGcvSnHPLHhrdBsQiaxJeQ90IQGVx0hxDIVre4raCEBkWMLiZUpKpTbjKLumnqu/8AwIQhHpO7G8SxKyTWWso3aSeWS/MungdLFCEcPLjJl4dfHbcfKRIkQhEmyQmIQGUUBiaqhFyfJNjCGzXjW8m0niK8p8k3GKfRczOl+EQj0sZqOPK+UNJcfAJMQhk0NmV8lTuUk/c7VSvqIRTH0jyGbBbEIaYWwGxCAw3EIQB//9k=\""
	// + "}";
	// // message = new StringBuilder().append("{}").toString();
	// client.sendMessage(message);
	// }
}
