
package com.wistron.license.logger.util;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

public class LogbackFileUtils {

	public static final String FRS_LOGGER = "ENROLL_LOGGER";
	private static FileAppender<ILoggingEvent> frsFileAppender;
	private static boolean frsInitialized = false;

	private static LoggerContext loggerContext;

	static {
		if (loggerContext == null) {
			loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		}
		if (!frsInitialized) {

			Logger frsLogger = loggerContext.getLogger(FRS_LOGGER);

			PatternLayoutEncoder encoder = new PatternLayoutEncoder();
			encoder.setContext(loggerContext);
			encoder.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS},%msg%n");
			encoder.start();

			frsFileAppender = new FileAppender<ILoggingEvent>();
			frsFileAppender.setContext(loggerContext);
			frsFileAppender.setName("MY_FILE_LOGGER");
			frsFileAppender.setAppend(true);
			frsFileAppender.setEncoder(encoder);
			frsLogger.addAppender(frsFileAppender);
			frsInitialized = true;

		}

	}

	public final static void start(String filePath) {
		stop();
		frsFileAppender.setFile(filePath);
		frsFileAppender.start();

	}

	public final static void stop() {
		if (frsFileAppender.isStarted()) {
			frsFileAppender.stop();
		}
	}

}
