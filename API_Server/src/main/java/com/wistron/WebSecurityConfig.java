package com.wistron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.wistron.license.jwthandler.EndpointAuthenticationCheckFilter;
import com.wistron.license.jwthandler.JWTAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final static Logger LOGGER = LoggerFactory.getLogger(WebSecurityConfig.class);

	@Autowired
	private JWTAuthenticationEntryPoint unauthorizedHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		LOGGER.info("[WebSecurityConfig][configure] Start to set WebSecurity");

		http
				// Disable CSRF protection since tokens are immune to it
				.csrf().disable()
				// If the user is not authenticated, returns 401
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
				// This is a stateless application, disable sessions
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				// Security policy
				.authorizeRequests()
				// Allow anonymous access to "/" path
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()// allow				
				.antMatchers("/log/**").permitAll()//				
				.antMatchers("/js/**").permitAll()//
				.antMatchers("/migration/**").permitAll()//
				.antMatchers("/token").permitAll()//
				.antMatchers("/status").permitAll()//
				.antMatchers("/").permitAll()//
				.antMatchers("/activate").permitAll()//
				.antMatchers("/**.html").permitAll()//
				.antMatchers("/css/**").permitAll()//
				.antMatchers("/fonts/**").permitAll()//
				.antMatchers("/js/**").permitAll()//
				.antMatchers("/template/**").permitAll()//
				.antMatchers("/notification/**").permitAll()
				// Any other request must be authenticated
				.anyRequest().authenticated().and()
				// Custom filter for authenticating users using tokens
				.addFilterBefore(new EndpointAuthenticationCheckFilter(), BasicAuthenticationFilter.class)
				// Disable resource caching
				.headers().cacheControl();
	}
}
