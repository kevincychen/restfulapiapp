(() => {
  const $migrationCode = $('#migrateCode');
  const $projectID = $('#projectID');
  const $resultCode = $('#resultCode');
  const $migrateButton = $('#migrationButton');

  axios.get('migration/getInfo').then((result) => {
    const data = result.data;
    $migrationCode.val(data.migrateCode);
    $projectID.val(data.projectId);
  }).catch(errorHandler);

  $migrateButton.on('click', () => {
    const data = { migrateCode: $resultCode.val() };
    $resultCode.attr('disabled', true);
    $migrateButton.attr('disabled', true);
    axios.post('/migration/migrate', data)
      .then(() => {
        $resultCode.removeAttr('disabled');
        $migrateButton.removeAttr('disabled');
        swal(
          $.i18n._('MIGRATE_SUCCESS'),
          $.i18n._('ACTIVATE_SUCCESS_MSG'),
          "success"
        ).then(() => {
          location.href = '/';
        });
      })
      .catch(() => {
        $resultCode.removeAttr('disabled');
        $migrateButton.removeAttr('disabled');
        errorHandler();
      });
  });
})()