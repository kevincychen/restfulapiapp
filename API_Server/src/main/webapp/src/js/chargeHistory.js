const chargeHistory = () => {
  const $paginationCharge = $('#paginationCharge');
  const $chargeBody = $('#chargeBody');
  const $chargeEndDate = $('#chargeEndDate');
  const $chargeStartDate = $('#chargeStartDate');
  const $chargeSearch = $('#chargeSearch');
  let historys = [];
  const pageItem = 10;
  const today = new Date();

  $chargeEndDate.val(today.format('YYYY-MM-DD'));
  $chargeStartDate.val(today.add(-30, 'days').format('YYYY-MM-DD'));

  const onPageClick = (event, page) => {
    $chargeBody.empty();
    const slicedHistory = historys.slice(((page - 1) * pageItem), page * pageItem);
    const $fragment = $(document.createDocumentFragment());
    slicedHistory.forEach(historyData => {
      const $tr = $('<tr />');
      const $tdNum = $('<td/>').text(historyData.id);
      const $tdTime = $('<td/>').text(historyData.time);
      $tr.append($tdNum); $tr.append($tdTime);
      $fragment.append($tr);
    });
    $chargeBody.append($fragment);
  }

  const searchHistory = () => {
    $chargeBody.empty();
    axios
      .get('/chargelog/counting', { params: { from: $chargeStartDate.val().date('YYYYMMDD'), end: $chargeEndDate.val().date('YYYYMMDD') } })
      .then((result) => {
        historys = result.data.logs.map((d, index) => { return { id: index, time: d }; });
        if (historys.length) {
          $paginationCharge.twbsPagination({
            totalPages: Math.ceil(historys.length / pageItem),
            visiblePages: 5,
            onPageClick,
            prev: '<',
            next: '>',
            first: '<<',
            last: '>>'
          });
          onPageClick(undefined, 1);
        } else {
          $chargeBody.append($('<td colspan="3">no data</td>'));
        }
      }).catch((e) => { console.error(e); });
  }

  $chargeSearch.on('click', searchHistory);
  searchHistory();
};
$(document).on('activateEvent', chargeHistory);