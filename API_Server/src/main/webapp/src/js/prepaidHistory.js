const prepaidHistory = () => {
  const $paginationPrepaid = $('#paginationPrepaid');
  const $prepaidBody = $('#prepaidBody');
  const $prepaidEndDate = $('#prepaidEndDate');
  const $prepaidStartDate = $('#prepaidStartDate');
  const $prepaidSearch = $('#prepaidSearch');
  let historys = [];
  const pageItem = 10;

  const onPageClick = (event, page) => {
    $prepaidBody.empty();
    const slicedHistory = historys.slice(((page - 1) * pageItem), page * pageItem);
    const $fragment = $(document.createDocumentFragment());
    slicedHistory.forEach(historyData => {
      const $tr = $('<tr />');
      const $tdNum = $('<td/>').text(historyData.id);
      const $tdSN = $('<td/>')
        .text(historyData.sn.substr(0, 20) + '...')
        .attr('title', historyData.sn);
      const $tdCount = $('<td/>').text(historyData.count);
      const $tdExpire = $('<td/>').text(historyData.expireTime);
      const $tdTime = $('<td/>').text(historyData.time);
      $tr.append($tdNum);
      $tr.append($tdTime);
      $tr.append($tdSN);
      $tr.append($tdCount);
      $tr.append($tdExpire);
      $fragment.append($tr);
    });
    $prepaidBody.append($fragment);
  }

  const searchHistory = () => {
    $prepaidBody.empty();
    axios
      .get('/chargelog/prepaid').then((result) => {
        historys = result.data.logs.map((d, index) => { d.id = (index + 1); return d; });
        if (historys.length) {
          $paginationPrepaid.twbsPagination({
            totalPages: Math.ceil(historys.length / pageItem),
            visiblePages: 5,
            onPageClick,
            prev: '<',
            next: '>',
            first: '<<',
            last: '>>'
          });
          onPageClick(undefined, 1);
        } else {
          $prepaidBody.append($('<td colspan="3">no data</td>'));
        }
      }).catch(errorHandler);
  }

  $prepaidSearch.on('click', searchHistory);
  searchHistory();

};

$(document).on('activateEvent', prepaidHistory);
