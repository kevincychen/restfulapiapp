const errorHandler = () => swal(
  $.i18n._('OOPS'),
  $.i18n._('OOPS_MSG'),
  'error',
);