const getStatus = () =>{
  const $activateStatus = $('#activateStatus');
  const $activateCount = $('#activateCount');
  const $activateExpire = $('#activateExpire');
  axios.get('/status').then((result) => {
    const data = result.data;
    if (data.activateStatus) {
      $activateCount.text(data.count);
      $activateStatus.text(data.activateStatus);
      $activateExpire.text(data.expireTime);
    } 
  }).catch(errorHandler);
};

$(document).on('activateEvent', getStatus);