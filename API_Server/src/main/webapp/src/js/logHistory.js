const logHistory = () => {
  const $paginationHistory = $('#paginationHistory');
  const $hisytoryBody = $('#hisytoryBody');
  const $historyEndDate = $('#historyEndDate');
  const $historyStartDate = $('#historyStartDate');
  const $historySearch = $('#historySearch');
  let historys = [];
  const pageItem = 10;
  const today = new Date();
  $historyEndDate.val(today.format('YYYY-MM-DD'));
  $historyStartDate.val(today.add(-30, 'days').format('YYYY-MM-DD'));
  const onPageClick = (event, page) => {
    $hisytoryBody.empty();
    const slicedHistory = historys.slice(((page - 1) * pageItem), page * pageItem);
    const $fragment = $(document.createDocumentFragment());
    slicedHistory.forEach(historyData => {
      const $tr = $('<tr />');
      const $tdNum = $('<td/>').text(historyData.id);
      const $tdLog = $('<td/>').text(`userID: ${historyData.userID}, Type: ${historyData.type}, Result: ${historyData.result}`);
      const $tdTime = $('<td/>').text(historyData.time);
      $tr.append($tdNum); $tr.append($tdLog); $tr.append($tdTime);
      $fragment.append($tr);
    });
    $hisytoryBody.append($fragment);
  }
  const searchHistory = () => {
    $hisytoryBody.empty();
    axios
      .get('/log', { params: { actions: '0,1,2', from: $historyStartDate.val().date('YYYYMMDD'), end: $historyEndDate.val().date('YYYYMMDD') } })
      .then((result) => {
        historys = result.data.logs.map((d, index) => { d.id = (index + 1); return d; });
        if (historys.length) {
          $paginationHistory.twbsPagination({
            totalPages: Math.ceil(historys.length / pageItem),
            visiblePages: 5,
            onPageClick,
            prev: '<',
            next: '>',
            first: '<<',
            last: '>>'
          });
          onPageClick(undefined, 1);
        } else {
          $hisytoryBody.append($('<td colspan="3">no data</td>'));
        }
      }).catch(errorHandler);
  }
  $historySearch.on('click', searchHistory);
  searchHistory();
};

$(document).on('activateEvent', logHistory);