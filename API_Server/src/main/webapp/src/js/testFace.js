const fr = new FileReader();
const $fileUpload = $('#testFile');
const $preview = $('#preview');
const $jsonReturn = $("#jsonreturn");
const $btnEnroll = $('#btnEnroll');
const $btnValidate = $('#btnValidate');
const $btnDelete = $('#btnDelete');
const $userID = $('#userID');
const $uploadControl = $('.uploadControl');
const $similarity = $('#similarity');
const $brightness = $('#brightness');
const $sharpness = $('#sharpness');
const $recoStatus = $('#recoStatus');
const $faceSize = $('#faceSize');

const eventStatus = {
  '0': 'success',
  '-1': 'user id error',
  '-2': 'not activated',
  '-3': 'params error',
  '-4': 'charge not enough',
  '-5': 'file too large',
  '-6': 'server error',
};

const uploadFile = (url, userID, imgBase64 = '') => new Promise((rs, rj) => {
  if (!userID || userID === '') {
    swal(
      'Alert!',
      'Please input userID!',
      'error'
    );
    return;
  }
  const faceImage = imgBase64.replace(/^data:image\/[a-z]+;base64,/, "");
  axios.post(url, { faceImage, userID }).then((data) => {
    rs(data);
  }).catch((e) => { rj(e); });
});

const clearText = () => {
  $jsonReturn.empty();
  $similarity.empty();
  $recoStatus.empty();
  $brightness.empty();
  $sharpness.empty();
  $faceSize.empty();
}

$fileUpload.on('change', (event) => {
  clearText();
  const file = event.target.files[0];
  const nameRex = (/\.(gif|jpg|jpeg|tiff|png)$/i);
  $uploadControl.attr('disabled', true);
  if (FileReader && file && nameRex.test(file.name)) {
    fr.onload = () => {
      $preview.css('background-image', `url("${fr.result}")`);
      $uploadControl.removeAttr('disabled');
      // uploadFile(fr.result);
    }
    fr.readAsDataURL(file);
  } else {
    $preview.css('background-image', 'none');
  }
});

$userID.on('change', (e) => {
  clearText();
  if (e.target.value) {
    $btnDelete.removeAttr('disabled');
  }
});

const errorHandler2 = (error) => {
  if (error.response.status === 400) {
    $jsonReturn.jJsonViewer(error.response.data);
    console.log(eventStatus);
    console.log(String(error.response.data.status));
    console.log(eventStatus[String(error.response.data.status)])
    $recoStatus.text(eventStatus[String(error.response.data.status)]);
  }
}

$btnEnroll.on('click', () => {
  uploadFile('/frs/enroll', $userID.val(), fr.result).then((result) => {
    const json = JSON.stringify(result.data);
    $jsonReturn.jJsonViewer(json);
    swal(
      'Enroll Success!',
      'Your Image has been enrolled!',
      'success'
    );
  }).catch(errorHandler2)
});

$btnValidate.on('click', () => {
  uploadFile('/frs/recognition', $userID.val(), fr.result).then((result) => {
    const json = JSON.stringify(result.data);
    $jsonReturn.jJsonViewer(json);
    $similarity.text(result.data.face.similarity);
    $brightness.text(result.data.face.quality.brightness);
    $sharpness.text(result.data.face.quality.sharpness);
    $faceSize.text(result.data.size);
    $recoStatus.text(eventStatus[String(result.data.status)]);
  }).catch(errorHandler2);
});

$btnDelete.on('click', () => {
  swal({
    title: $.i18n._('DELETE_MSG_SURE'),
    text: $.i18n._('DELETE_MSG_TEXT'),
    type: 'warning',
    showCancelButton: true,
    // confirmButtonColor: '#3085d6',
    // cancelButtonColor: '#d33',
    confirmButtonText: $.i18n._('DELETE'),
    cancelButtonText: $.i18n._('CANCEL'),
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      uploadFile('/frs/delete', $userID.val()).then((result) => {
        swal(
          $.i18n._('DELETE'),
          $.i18n._('DELETE_SUCCESS_MSG'),
          'success'
        );
        const json = JSON.stringify(result.data);
        $jsonReturn.jJsonViewer(json);
      }).catch(errorHandler)
    }
  });
});