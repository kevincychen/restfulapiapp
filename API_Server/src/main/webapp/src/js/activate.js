(() => {
  const $activateButton = $('#activateButton');
  const $activateCode = $('#activateCode');
  const disableBtn = (bool) => {
    if (bool) {
      $activateButton.attr('disabled', true);
      $activateCode.attr('disabled', true);
    } else {
      $activateButton.removeAttr('disabled');
      $activateCode.removeAttr('disabled');
    }
  };
  $activateButton.on('click', () => {
    disableBtn(true);
    const code = $('#activateCode').val();
    axios.post('activate', { code }).then(() => {
      swal(
        $.i18n._('ACTIVATE_SUCCESS'),
        $.i18n._('ACTIVATE_SUCCESS_MSG'),
        "success"
      ).then(() => {
        location.href = '/';
      })
    }).catch(() => {
      swal(
        $.i18n._('OOPS'),
        $.i18n._('ACTIVATE_ERROR_MSG'),
        'error');
      disableBtn(false);
    });
  });
})();
