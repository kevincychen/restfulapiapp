const $tags = document.getElementById('recipients');
const $host = $('#host');
const $port = $('#port');
const $senderEmail = $('#senderEmail');
const $senderPassword = $('#senderPassword');
const $recipients = $('#recipients');
const $btnTest = $('#btnTest');
const $btnSave = $('#btnSave');

const successSaveHandler = () => swal(
  $.i18n._('SAVE_SUCCESS'),
  $.i18n._('SAVE_SUCCESS_MSG'),
  'success'
);

const successTestHandler = () => swal(
  $.i18n._('TEST_SUCCESS'),
  $.i18n._('TEST_SUCCESS_MSG'),
  'success'
);

const getSetting = () => {
  axios.get('notification/getEmailInfo').then((result) => {
    const data = result.data;
    $host.val(data.host);
    $port.val(data.port);
    $senderEmail.val(data.senderEmail);
    $senderPassword.val(data.senderPassword);
    $recipients.val(data.recipients);
    tagsInput($tags);
  }).catch(errorHandler);
};

const extractDomInput = () => ({
  host: $host.val(),
  port: $port.val(),
  senderEmail: $senderEmail.val(),
  senderPassword: $senderPassword.val(),
  recipients: $recipients.val(),
});

const saveSetting = () => {
  const data = extractDomInput();
  axios.post('notification/saveEmailInfo', data).then(successSaveHandler).catch(errorHandler);
};

const testSetting = () => {
  const data = extractDomInput();
  axios.post('notification/testSendEmail', data).then(successTestHandler).catch(errorHandler);
};

const normolizePort = (e) => {
  if (parseInt(e.target.value) > 65535) {
    e.target.value = '65535'
  }
}

getSetting();

$btnSave.on('click', saveSetting);
$btnTest.on('click', testSetting);
$port.on('blur', normolizePort);