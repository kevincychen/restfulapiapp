
(() => {
  const loadLang = () => {
    $.i18n.unload();
    if (localStorage.language && localization && localization.hasOwnProperty(localStorage.language)) {
      $.i18n.load(localization[localStorage.language]);
    } else {
      localStorage.language = 'en';
      $.i18n.load(localization.en);
    }
  };
  const translate = (lang) => {
    if (lang) {
      localStorage.language = lang;
    }
    loadLang();
    $('[t]').each((i, ele) => {
      const $this = $(ele);
      const attr = $this.attr('t');
      $this.text($.i18n._(attr));
    });
  };
  const documentReady = () => {
    const $langBtn = $('.langBtn');
    axios.get('/token').then((result) => {
      if (location.pathname === '/activateCode.html' || location.pathname === '/migration.html') {
        location.pathname = '';
        return;
      }
      axios.defaults.headers.common['Authorization'] = `Bearer ${result.data.token}`;
      $(document).trigger('activateEvent');
    }).catch((e) => {
      if (e.response.status === 400 && location.pathname !== '/activateCode.html' && location.pathname !== '/migration.html') {
        location.pathname = '/activateCode.html';
      }
    });
    $langBtn.on('click', function (event) {
      const lang = $(this).attr('lang');
      translate(lang);
    });
    translate();
    $('#cover').fadeOut();
  };
  $(document).ready(documentReady);
})()
