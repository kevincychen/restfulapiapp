(() => {
  const $submitBtn = $('#snSubmit');
  const $snCode = $('#snCode');
  const disableBtn = (bool) => {
    if (bool) {
      $snCode.attr('disabled', true);
      $submitBtn.attr('disabled', true);
    } else {
      $snCode.removeAttr('disabled');
      $submitBtn.removeAttr('disabled');
    }
  };
  $submitBtn.on('click', () => {
    disableBtn(true);
    const sn = $snCode.val();
    axios.post('/prepaid', { sn }).then(() => {
      swal(
        $.i18n._('PREPAID_SUCCESS'),
        $.i18n._('PREPAID_SUCCESS_MSG'),
        "success"
      ).then(() => {
        location.href = '/';
      });

    }).catch(() => {
      swal(
        $.i18n._('PREPAID_ERROR'),
        $.i18n._('PREPAID_ERROR_MSG'),
        'error'
      );
      disableBtn(false);
    });
  });
})();
