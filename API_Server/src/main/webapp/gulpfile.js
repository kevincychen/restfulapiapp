const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const jade = require('gulp-jade');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const distFolder = 'pages';

// Compile Sass & Inject Into Browser
gulp.task('sass', function () {
  return gulp.src([
    'src/scss/*.scss',
    'src/scss/*.css'])
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(gulp.dest(`${distFolder}/css`))
    .pipe(browserSync.stream());
});

// Move JS Files to src/js
gulp.task('libjs', function () {
  return gulp.src([
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/axios/dist/axios.min.js',
    'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
    'node_modules/tags-input/tags-input.js',
    'node_modules/twbs-pagination/jquery.twbsPagination.min.js',
    'node_modules/date-format-lite/dist/index-min.js',
    'node_modules/es6-shim/es6-shim.min.js'
  ]).pipe(gulp.dest(`${distFolder}/js`));
});

gulp.task('js', () => gulp
  .src('src/js/*.js')
  .pipe(babel({
    presets: ['env']
  }))
  .pipe(uglify())
  .pipe(gulp.dest(`${distFolder}/js`))
  .pipe(browserSync.stream())
);

// Move Fonts to src/fonts
gulp.task('fonts', function () {
  return gulp.src('node_modules/font-awesome/fonts/*')
    .pipe(gulp.dest(`${distFolder}/fonts`))
})

// Move Font Awesome CSS to src/css
gulp.task('libcss', () => gulp
  .src([
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/sweetalert2/dist/sweetalert2.min.css',
    'node_modules/tags-input/tags-input.css',
  ]).pipe(gulp.dest(`${distFolder}/css`))
)

gulp.task('template', () => gulp
  .src('src/**/*.jade')
  .pipe(jade())
  .pipe(gulp.dest(`${distFolder}`))
  .pipe(browserSync.stream())
)

// Watch Sass & Serve
gulp.task('server', ['default'], function () {
  browserSync.init({
    server: `./${distFolder}`
  });
  gulp.watch('src/js/*.js', ['js']);
  gulp.watch(['src/scss/*'], ['sass']);
  gulp.watch("src/**/*.jade", ['template']);
});

gulp.task('default', ['sass', 'libjs', 'js', 'libcss', 'fonts', 'template']);
