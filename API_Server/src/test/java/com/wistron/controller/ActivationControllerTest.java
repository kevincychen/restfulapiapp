package com.wistron.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.UUID;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.wistron.common.AbstractTest;
import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.common.ActivationResponseVO;
import com.wistron.license.common.PrepaidInfoVO;
import com.wistron.license.common.PrepaidResponseVO;
import com.wistron.license.common.ResponseMsg;
import com.wistron.license.controller.ActivationController;
import com.wistron.license.jwthandler.TokenAuthenticationService;
import com.wistron.license.service.ActivationService;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.HttpService;

public class ActivationControllerTest extends AbstractTest {

	@Rule
	public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

	@Mock
	private EncryptService encryptService;

	@Mock
	private HttpService httpService;

	@Autowired
	@InjectMocks
	private ActivationService activationService;

	@InjectMocks
	@Autowired
	private ActivationController activationController;

	@Value("${token.file}")
	private String tokenFilePath;

	private HttpHeaders headers;
	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(activationController).build();
		MockitoAnnotations.initMocks(this);
		environmentVariables.set("PROJECT_ID", "value");

	}

	@Test
	public void testActivateErrorWithCodeInvalidated() throws Exception {
		final Gson gson = new Gson();
		ActivationInfoVO requestVoWithEmpty = new ActivationInfoVO();
		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVoWithEmpty)))
				.andExpect(status().isBadRequest());

		ActivationInfoVO requestVoWithTooLong = new ActivationInfoVO();
		requestVoWithTooLong.setCode("AAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBCCCCCCCCCCCCCCCCCCDDDDDDDDDDDDDD");
		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVoWithTooLong)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testActivateErrorWithActivate() throws Exception {
		final Gson gson = new Gson();
		ActivationInfoVO requestVO = new ActivationInfoVO();
		requestVO.setCode("1234567890");
		when(this.encryptService.isActivated()).thenReturn(true);
		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testActivateErrorWithNoneProjectId() throws Exception {
		final Gson gson = new Gson();
		ActivationInfoVO requestVO = new ActivationInfoVO();
		requestVO.setCode("1234567890");
		// when(this.encryptService.isActivated()).thenReturn(false);
		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testActivateErrorWithSNIsEmpty() throws Exception {

		final Gson gson = new Gson();
		ActivationInfoVO requestVO = new ActivationInfoVO();
		requestVO.setCode("1234567890");

		ActivationResponseVO responseVO = new ActivationResponseVO();
		responseVO.setSnsecret("");
		responseVO.setMessage("");
		String returnStr = gson.toJson(responseVO);
		when(this.httpService.sendCodeToServer(any(ActivationInfoVO.class))).thenReturn(returnStr);

		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testActivateErrorWithInitError() throws Exception {

		final Gson gson = new Gson();
		ActivationInfoVO requestVO = new ActivationInfoVO();
		requestVO.setCode("1234567890");

		ActivationResponseVO responseVO = new ActivationResponseVO();
		responseVO.setSnsecret("123456");
		responseVO.setMessage("OK");
		String returnStr = gson.toJson(responseVO);
		when(this.httpService.sendCodeToServer(any(ActivationInfoVO.class))).thenReturn(returnStr);
		when(this.encryptService.initModule(anyString(), anyString())).thenReturn(false);
		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testActivateWithSuccess() throws Exception {

		final Gson gson = new Gson();
		ActivationInfoVO requestVO = new ActivationInfoVO();
		requestVO.setCode("1234567890");

		ActivationResponseVO responseVO = new ActivationResponseVO();
		responseVO.setSnsecret("123456");
		responseVO.setMessage("OK");
		String returnStr = gson.toJson(responseVO);
		when(this.httpService.sendCodeToServer(any(ActivationInfoVO.class))).thenReturn(returnStr);
		when(this.encryptService.initModule(anyString(), anyString())).thenReturn(true);

		mockMvc.perform(post("/activate").headers(headers).content(gson.toJson(requestVO))).andExpect(status().isOk());
	}

	@Test
	public void testGetTokenNotActivated() throws Exception {
		// TokenResponseVO mockResponseVO = new TokenResponseVO("testToken",
		// ResponseMsg.OK.toString());

		when(this.encryptService.isActivated()).thenReturn(false);
		mockMvc.perform(get("/token").headers(headers)).andExpect(status().isBadRequest());

	}

	// For token api
	@Test
	public void testGetToken() throws Exception {

		// save jwt
		TokenAuthenticationService.saveJWT("123456", tokenFilePath);
		when(this.encryptService.isActivated()).thenReturn(true);
		// success
		mockMvc.perform(get("/token").headers(headers)).andExpect(status().isOk());

	}

	// // For status api
	@Test
	public void testGetStatusSuccess() throws Exception {
		when(this.encryptService.isActivated()).thenReturn(true);
		when(this.encryptService.getUsageCount()).thenReturn(10L);

		mockMvc.perform(get("/status").headers(headers)).andExpect(status().isOk());
	}

	@Test
	public void testPrepaidErrorWithParamError() throws Exception {
		PrepaidResponseVO mockResponseVO = new PrepaidResponseVO();
		mockResponseVO.setMessage(ResponseMsg.OK.toString());

		final Gson gson = new Gson();
		String jsonBody = gson.toJson(new PrepaidInfoVO());
		mockMvc.perform(post("/prepaid").headers(headers).content(jsonBody)).andExpect(status().isBadRequest());

		PrepaidInfoVO prepaidInfoVO = new PrepaidInfoVO();
		StringBuilder str = new StringBuilder();
		for (int i = 0; i <= 100; i++) {
			str.append(UUID.randomUUID().toString());
		}

		prepaidInfoVO.setSn(str.toString());
		mockMvc.perform(post("/prepaid").headers(headers).content(gson.toJson(prepaidInfoVO)))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testPrepaidSuccess() throws Exception {

		final Gson gson = new Gson();
		PrepaidInfoVO prepaidInfoVO = new PrepaidInfoVO();
		StringBuilder str = new StringBuilder();
		for (int i = 0; i <= 1; i++) {
			str.append(UUID.randomUUID().toString());
		}
		prepaidInfoVO.setSn(str.toString());
		when(this.encryptService.addPurchase(anyString())).thenReturn(100L);
		when(this.encryptService.isActivated()).thenReturn(true);
		mockMvc.perform(post("/prepaid").headers(headers).content(gson.toJson(prepaidInfoVO)))
				.andExpect(status().isOk());

	}

	@Test
	public void testPrepaidError() throws Exception {

		final Gson gson = new Gson();
		PrepaidInfoVO prepaidInfoVO = new PrepaidInfoVO();
		StringBuilder str = new StringBuilder();
		for (int i = 0; i <= 1; i++) {
			str.append(UUID.randomUUID().toString());
		}
		prepaidInfoVO.setSn(str.toString());
		when(this.encryptService.addPurchase(anyString())).thenReturn(-1L);
		when(this.encryptService.isActivated()).thenReturn(true);
		mockMvc.perform(post("/prepaid").headers(headers).content(gson.toJson(prepaidInfoVO)))
				.andExpect(status().isBadRequest());

	}

}
