package com.wistron.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.wistron.common.AbstractTest;
import com.wistron.license.common.EmailContextVO;
import com.wistron.license.common.EmailResponseVO;
import com.wistron.license.common.Face;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.common.FrsResponseVO;
import com.wistron.license.common.FrsStatus;
import com.wistron.license.common.Quality;
import com.wistron.license.controller.FrsController;
import com.wistron.license.securityModule.util.UsageSign;
import com.wistron.license.service.EmailService;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.FrsMsgTransportService;
import com.wistron.license.service.FrsService;

public class FrsControllerTest extends AbstractTest {

	@Mock
	private EmailService emailService;

	@Mock
	private EncryptService encryptService;

	@Mock
	private FrsMsgTransportService frsMsgTransportService;

	@Autowired
	@InjectMocks
	private FrsService mockFrsService;

	@InjectMocks
	@Autowired
	private FrsController frsController;

	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	private HttpHeaders headers;

	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(frsController).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testEnrollErrorWithParam() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO1 = new FrsRequestVO();

		mockMvc.perform(post("/frs/enroll").headers(headers).content(gson.toJson(requestVO1)))
				.andExpect(status().isBadRequest());

	} 

	@Test
	public void testEnrollSuccess() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		FrsResponseVO frsResponseVO = new FrsResponseVO();
		Face face = new Face();
		face.setSimilarity(100f);

		Quality quality = new Quality();
		quality.setBrightness(10f);
		quality.setSharpness(10f);

		face.setQuality(quality);
		frsResponseVO.setFace(face);
		frsResponseVO.setStatus(FrsStatus.SUCCESS.getValue());

		when(this.encryptService.isActivated()).thenReturn(true);
		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenReturn(gson.toJson(frsResponseVO));

		mockMvc.perform(post("/frs/enroll").headers(headers).content(jsonBody)).andExpect(status().isOk());

	}

	@Test
	public void testEnrollErrorWithTimeout() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		FrsResponseVO frsResponseVO = new FrsResponseVO();
		frsResponseVO.setStatus(FrsStatus.SERVER_ERROR.getValue());

		when(this.encryptService.isActivated()).thenReturn(true); 
		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenReturn(gson.toJson(frsResponseVO));

		mockMvc.perform(post("/frs/enroll").headers(headers).content(jsonBody)).andExpect(status().isForbidden());

	}

	@Test
	public void testEnrollErrorWithNoneActivate() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		when(this.encryptService.isActivated()).thenReturn(false);

		mockMvc.perform(post("/frs/enroll").headers(headers).content(jsonBody)).andExpect(status().isBadRequest());

	}

	@Test
	public void testRecognitionErrorWithParam() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO1 = new FrsRequestVO();

		mockMvc.perform(post("/frs/recognition").headers(headers).content(gson.toJson(requestVO1)))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testRecognitionErrorWithNoUsage() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		UsageSign usageSign = new UsageSign();
		usageSign.setCount(0);
		usageSign.setSign("abcde");
		when(this.encryptService.isActivated()).thenReturn(true);
		when(this.encryptService.getUsageSign(anyString(), anyString())).thenReturn(usageSign);

		mockMvc.perform(post("/frs/recognition").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testRecognitionErrorWithFrs() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		FrsResponseVO frsResponseVO = new FrsResponseVO();
		Face face = new Face();
		face.setSimilarity(100f);

		Quality quality = new Quality();
		quality.setBrightness(10f);
		quality.setSharpness(10f);

		face.setQuality(quality);
		frsResponseVO.setFace(face);
		frsResponseVO.setStatus(3);

		UsageSign usageSign = new UsageSign();
		usageSign.setCount(510);
		usageSign.setSign("abcde");
		when(this.encryptService.isActivated()).thenReturn(true);

		when(this.encryptService.getUsageSign(anyString(), anyString())).thenReturn(usageSign);
		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenReturn(gson.toJson(frsResponseVO));

		mockMvc.perform(post("/frs/recognition").headers(headers).content(jsonBody)).andExpect(status().isForbidden());

	}

	@Test
	public void testRecognitionErrorWithNoneActivate() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		when(this.encryptService.isActivated()).thenReturn(false);

		mockMvc.perform(post("/frs/recognition").headers(headers).content(jsonBody)).andExpect(status().isBadRequest());

	}

	@Test
	public void testRecognitionSuccess() throws Exception {

		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		requestVO.setFaceImage("12233");
		String jsonBody = gson.toJson(requestVO);

		FrsResponseVO frsResponseVO = new FrsResponseVO();
		Face face = new Face();
		face.setSimilarity(100f);

		Quality quality = new Quality();
		quality.setBrightness(10f);
		quality.setSharpness(10f);

		face.setQuality(quality);
		frsResponseVO.setFace(face);
		frsResponseVO.setStatus(FrsStatus.SUCCESS.getValue());

		UsageSign usageSign = new UsageSign();
		usageSign.setCount(500);
		usageSign.setSign("abcde");

		EmailResponseVO emailResponseVO = new EmailResponseVO();
		when(this.encryptService.isActivated()).thenReturn(true);
		when(this.encryptService.getUsageSign(anyString(), anyString())).thenReturn(usageSign);
		when(this.emailService.sendEmailFromFile(any(EmailContextVO.class))).thenReturn(emailResponseVO);
		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenReturn(gson.toJson(frsResponseVO));

		mockMvc.perform(post("/frs/recognition").headers(headers).content(jsonBody)).andExpect(status().isOk());

	}

	@Test 
	public void testDeleteSuccess() throws Exception {
		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		String jsonBody = gson.toJson(requestVO);

		FrsResponseVO frsResponseVO = new FrsResponseVO();
		frsResponseVO.setStatus(FrsStatus.SUCCESS.getValue());

		when(this.encryptService.isActivated()).thenReturn(true);

		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenReturn(gson.toJson(frsResponseVO));

		mockMvc.perform(post("/frs/delete").headers(headers).content(jsonBody)).andExpect(status().isOk());
	}

	@Test 
	public void testDeleteErrorWithException() throws Exception {
		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123445");
		String jsonBody = gson.toJson(requestVO);

		
		when(this.encryptService.isActivated()).thenReturn(true);

		when(this.frsMsgTransportService.sendFrsMsg(any(FrsRequestVO.class))).thenThrow(new IOException());

		mockMvc.perform(post("/frs/delete").headers(headers).content(jsonBody)).andExpect(status().isForbidden());
	}

	
	@Test
	public void testDeleteErrorWithParam() throws Exception {
		final Gson gson = new Gson();
		FrsRequestVO requestVO = new FrsRequestVO();

		String jsonBody = gson.toJson(requestVO);

		mockMvc.perform(post("/frs/delete").headers(headers).content(jsonBody)).andExpect(status().isBadRequest());
	}

	//
	// @Test
	// public void testDeleteBadRequestError() throws Exception {
	// FrsResponseVO mockResponseVO = new FrsResponseVO();
	// mockResponseVO.setStatus(-5);
	// when(mockFrsService.delete(any(FrsRequestVO.class))).thenReturn(mockResponseVO);
	//
	// final Gson gson = new Gson();
	// String jsonBody = gson.toJson(new FrsRequestVO());
	//
	// mockMvc.perform(post("/frs/delete").headers(headers).content(jsonBody)).andExpect(status().isBadRequest())
	// .andDo(print()).andReturn().getResponse().getContentAsString();
	// }
	//
	// @Test
	// public void testDeleteForbidden() throws Exception {
	// FrsResponseVO mockResponseVO = new FrsResponseVO();
	// mockResponseVO.setStatus(-6);
	// when(mockFrsService.delete(any(FrsRequestVO.class))).thenReturn(mockResponseVO);
	//
	// final Gson gson = new Gson();
	// String jsonBody = gson.toJson(new FrsRequestVO());
	//
	// mockMvc.perform(post("/frs/delete").headers(headers).content(jsonBody)).andExpect(status().isForbidden())
	// .andDo(print()).andReturn().getResponse().getContentAsString();
	// }

}
