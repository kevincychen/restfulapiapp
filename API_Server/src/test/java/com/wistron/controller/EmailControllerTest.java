
package com.wistron.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.wistron.common.AbstractTest;
import com.wistron.license.common.EmailInfoVO;
import com.wistron.license.controller.EmailController;

public class EmailControllerTest extends AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	// @Mock
	// private EmailService emailService;

	@InjectMocks
	@Autowired
	private EmailController emailController;

	@Value("${token.file}")
	private String tokenFilePath;

	@Value("${mail.file}")
	private String mailPath;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	private HttpHeaders headers;
	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(emailController).build();

	}

	@Test
	public void testGetMailInfo() throws Exception {
		File mailFile = new File(mailPath);

		if (mailFile.exists()) {
			FileUtils.forceDelete(mailFile);
		}
		mockMvc.perform(get("/notification/getEmailInfo").headers(headers)).andExpect(status().isOk());

	}

	@Test
	public void testGetMailInfoWithFileExist() throws Exception {
		File mailFile = new File(mailPath);

		if (mailFile.exists()) {
			FileUtils.forceDelete(mailFile);
		}
		EmailInfoVO emailInfo = new EmailInfoVO("localhost", 23, "aaa@aa.aa", "12345", "louis@wistrion.com");
		Gson gson = new Gson();
		FileUtils.writeStringToFile(mailFile, gson.toJson(emailInfo), "UTF-8");
		mockMvc.perform(get("/notification/getEmailInfo").headers(headers)).andExpect(status().isOk());
		FileUtils.forceDelete(mailFile);
	}

	@Test
	public void testSaveMailInfo() throws Exception {
		File mailFile = new File(mailPath);

		if (mailFile.exists()) {
			FileUtils.forceDelete(mailFile);
		}
		EmailInfoVO emailInfo = new EmailInfoVO("localhost", 23, "aaa@aa.aa", "12345", "louis@wistrion.com");
		Gson gson = new Gson();

		mockMvc.perform(post("/notification/saveEmailInfo").headers(headers).content(gson.toJson(emailInfo)))
				.andExpect(status().isOk());

		FileUtils.forceDelete(mailFile);
	}

	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
