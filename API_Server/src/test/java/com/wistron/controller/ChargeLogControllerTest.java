package com.wistron.controller;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.wistron.common.AbstractTest;
import com.wistron.license.controller.ChargeLogController;
import com.wistron.license.securityModule.util.ChargeLog;
import com.wistron.license.securityModule.util.Log;
import com.wistron.license.securityModule.util.Purchase;
import com.wistron.license.securityModule.util.PurchaseHistory;
import com.wistron.license.service.ChargeLogService;
import com.wistron.license.service.EncryptService;

public class ChargeLogControllerTest extends AbstractTest {

	@Mock
	private EncryptService encryptService;

	@Autowired
	@InjectMocks
	private ChargeLogService mockChargeLogService;

	@InjectMocks
	@Autowired
	private ChargeLogController chargeLogController;



	@LocalServerPort 
	private int port;

	private HttpHeaders headers;
	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(chargeLogController).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetCountingLogErrorWithDateError() throws Exception {

		mockMvc.perform(get("/chargelog/counting?from=20180112&end=20180105").headers(headers))
				.andExpect(status().isBadRequest());

	}

	@Test
	public void testGetCountingLogSuccess() throws Exception {
		Log log1 = new Log();
		log1.setSign("123");
		log1.setTimestamp(123456l);
		Log log2 = new Log();
		log2.setSign("123");
		log2.setTimestamp(123456l);
		List<Log> logs = new ArrayList<Log>();
		logs.add(log1);
		logs.add(log2);
		ChargeLog chargeLog = new ChargeLog();
		chargeLog.setList(logs);
		chargeLog.setStatus(true);
		when(this.encryptService.getChargebackHistory(anyLong(), anyLong())).thenReturn(chargeLog);

		mockMvc.perform(get("/chargelog/counting?from=20180102&end=20180105").headers(headers))
				.andExpect(status().isOk());

	}

	@Test
	public void testGetPrepaidLogSuccess() throws Exception {

		PurchaseHistory history = new PurchaseHistory();
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases.add(new Purchase("123", 100, 8569856985698L, 8569856985698L));
		purchases.add(new Purchase("123", 100, 8569856985698L, 8569856985698L));
		history.setList(purchases);
		history.setStatus(true);
		when(this.encryptService.getPurchaseList()).thenReturn(history);
		mockMvc.perform(get("/chargelog/prepaid").headers(headers)).andExpect(status().isOk());
	}

}
