
package com.wistron.controller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.wistron.common.AbstractTest;
import com.wistron.license.common.MigrationRequestVO;
import com.wistron.license.controller.MigrationController;
import com.wistron.license.service.EncryptService;
import com.wistron.license.service.MigrationService;

public class MigrationControllerTest extends AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Rule
	public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

	@Mock
	private EncryptService encryptService;

	@InjectMocks
	@Autowired
	private MigrationService migrationService;

	@InjectMocks
	@Autowired
	private MigrationController migrationController;

	@Value("${token.file}")
	private String tokenFilePath;

	private HttpHeaders headers;
	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(migrationController).build();
		MockitoAnnotations.initMocks(this);
		environmentVariables.set("PROJECT_ID", "value");

	}

	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====

	@Test
	public void testGetMigrationInfoSuccess() throws Exception {

		when(this.encryptService.getMigrationInfo()).thenReturn("12345");
		mockMvc.perform(get("/migration/getInfo").headers(headers)).andExpect(status().isOk());

	}

	@Test
	public void testGetMigrationInfoErrorWithException() throws Exception {

		when(this.encryptService.getMigrationInfo()).thenThrow(new Exception());
		mockMvc.perform(get("/migration/getInfo").headers(headers)).andExpect(status().isBadRequest());

	}

	@Test
	public void testMigrateSuccess() throws Exception {

		when(this.encryptService.migrate(anyString())).thenReturn(true);
		MigrationRequestVO requestVO = new MigrationRequestVO();
		requestVO.setMigrateCode("12345");
		Gson gson = new Gson();
		mockMvc.perform(post("/migration/migrate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isOk());

	}

	@Test
	public void testMigrateErrorWithException() throws Exception {

		when(this.encryptService.migrate(anyString())).thenReturn(true);
		MigrationRequestVO requestVO = new MigrationRequestVO();
		requestVO.setMigrateCode("12345");
		Gson gson = new Gson();
		when(this.encryptService.migrate(anyString())).thenThrow(new Exception());

		mockMvc.perform(post("/migration/migrate").headers(headers).content(gson.toJson(requestVO)))
				.andExpect(status().isBadRequest());
	}

	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
