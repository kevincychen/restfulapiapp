package com.wistron.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.wistron.common.AbstractTest;
import com.wistron.license.controller.FrsLogController;

public class FrsLogControllerTest extends AbstractTest {

	@Autowired
	private FrsLogController frsLogController;

	@Value("${log_path}")
	private String logPath;

	@Value("${temp_path}")
	private String tempPath;

	private HttpHeaders headers;
	private String jwt = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlMmMxMTJlNS1iNWQ5LTQ1ZmUtYjZkMy00ZGNiZjhhNGZjZjAiLCJzdWIiOiJUZXN0IiwiaWF0IjoxNTE1NjU2MTMxfQ.5SKMddhhL0lQNiaHfpFu1kYwymfK3EYwn9O0X4ey7pQ";

	@Before
	public void setUp() throws Exception {
		headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", jwt);

		mockMvc = MockMvcBuilders.standaloneSetup(frsLogController).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testQueryLogSuccess() throws Exception {
		File file1 = new File(logPath + "frs/20180105.csv");
		File file2 = new File(logPath + "frs/20180106.csv");
		if (file1.exists()) {
			FileUtils.forceDelete(file1);
		}
		if (file2.exists()) {
			FileUtils.forceDelete(file2);
		}

		FileUtils.writeStringToFile(file1, "2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS", "UTF-8");
		FileUtils.writeStringToFile(file2, "2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS", "UTF-8");

		mockMvc.perform(get("/log?actions=0,1,2&from=20180105&end=20180106").headers(headers))
				.andExpect(status().isOk());

	}

	@Test
	public void testQueryLogSuccessOver100() throws Exception {
		File file1 = new File(logPath + "frs/20180105.csv");
		File file2 = new File(logPath + "frs/20180106.csv");

		FileUtils.forceDeleteOnExit(file1);

		FileUtils.forceDeleteOnExit(file2);

		StringBuilder str1 = new StringBuilder();
		StringBuilder str2 = new StringBuilder();
		for (int i = 0; i <= 501; i++) {
			str1.append("2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS");
			str2.append("2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS");
			if (i < 501) {
				str1.append("\n");
				str2.append("\n"); 

			}
		}
		FileUtils.writeStringToFile(file1, str1.toString(), "UTF-8");
		FileUtils.writeStringToFile(file2, str2.toString(), "UTF-8");

		mockMvc.perform(get("/log?actions=0,1,2&from=20180105&end=20180106").headers(headers))
				.andExpect(status().isOk());

	}

	@Test
	public void testQueryLogErrorWithActionError() throws Exception {
		mockMvc.perform(get("/log?actions=3,4&from=20180105&end=20180106").headers(headers))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testQueryLogErrorWithDateError() throws Exception {
		mockMvc.perform(get("/log?actions=0,1,2&from=20180108&end=20180106").headers(headers))
				.andExpect(status().isBadRequest());
	}

	//
	@Test
	public void testExportLogSuccess() throws Exception {
		File file1 = new File(logPath + "frs/20180105.csv");
		File file2 = new File(logPath + "frs/20180106.csv");
		FileUtils.forceDeleteOnExit(file1);

		FileUtils.forceDeleteOnExit(file2);

		StringBuilder str1 = new StringBuilder();
		StringBuilder str2 = new StringBuilder();
		for (int i = 0; i <= 501; i++) {
			str1.append("2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS");
			str2.append("2018-02-02 22:58:11.522,123456,ENROLL,SUCCESS");
			if (i < 501) {
				str1.append("\n");
				str2.append("\n");

			}
		}
		FileUtils.writeStringToFile(file1, str1.toString(), "UTF-8");
		FileUtils.writeStringToFile(file2, str2.toString(), "UTF-8");

		mockMvc.perform(get("/log/export?from=20180105&end=20180130").headers(headers)).andExpect(status().isOk());

		final String zipFile = "temp/";
		File zFile = new File(this.tempPath + zipFile);
		if (zFile.exists()) {
			FileUtils.deleteDirectory(zFile);
		}
 
	}
	//
	// @Test
	// public void testExportLogError() throws Exception {
	// when(mockLogService.exportLog(any(Date.class),
	// any(Date.class))).thenReturn(null);
	// mockMvc.perform(get("/log/export?from=20180105&end=20180130").headers(headers))
	// .andExpect(status().isBadRequest()).andDo(print()).andReturn().getResponse().getContentAsString();
	// }
}
