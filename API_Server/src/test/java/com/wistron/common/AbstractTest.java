
package com.wistron.common;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

/**
 *
 * Page/Class Name: AbstractTest Title: Description: Copyright: Copyright(c)
 * 2004 Wistron Corportion. All Rights Reserved. I Company: Wistron Corportion
 * author: louis Create Date: 2018???1???8??? Last Modifier: louis Last Modify Date:
 * 2018???1???8??? Version 1.0
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Ignore
@ActiveProfiles(profiles = "dev,false")
public class AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Autowired
	protected MockMvc mockMvc;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (???init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	protected GenerateCodeVO generateCode() throws ClientProtocolException, IOException {
		String api = "https://1jun5hdpmj.execute-api.us-east-1.amazonaws.com/dev/generateCode";
		HttpPost request = new HttpPost(api);
		final Gson gson = new Gson();
		GenerateCodeRequestVO requestVO = new GenerateCodeRequestVO("fdddddf", "feuddffffddddeeee");
		StringEntity params = new StringEntity(gson.toJson(requestVO));
		request.addHeader("content-type", "application/json");
		request.setEntity(params);
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = httpClient.execute(request);

		final GenerateCodeVO generateCodeVO = gson.fromJson(EntityUtils.toString(response.getEntity()),
				GenerateCodeVO.class);
		return generateCodeVO;

	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
