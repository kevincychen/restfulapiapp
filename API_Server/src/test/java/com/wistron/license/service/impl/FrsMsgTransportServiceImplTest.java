
package com.wistron.license.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import com.wistron.common.AbstractTest;
import com.wistron.license.common.FrsRequestVO;
import com.wistron.license.service.FrsMsgTransportService;
import com.wistron.license.udp.UdpClient;

public class FrsMsgTransportServiceImplTest extends AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Mock
	private UdpClient udpClient;

	@InjectMocks
	@Autowired
	private FrsMsgTransportService frsMsgTransportService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	@Test
	public void testSendFrsMsg() throws IOException {

		FrsRequestVO requestVO = new FrsRequestVO();
		requestVO.setUserID("123456");
		requestVO.setAction("add");
		requestVO.setFaceImage("123456");
		when(this.udpClient.sendMessage()).thenReturn("status:0");
		assertEquals("status:0", this.frsMsgTransportService.sendFrsMsg(requestVO));
	}
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
