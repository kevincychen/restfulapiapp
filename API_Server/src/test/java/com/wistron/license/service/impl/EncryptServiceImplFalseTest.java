
package com.wistron.license.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import com.wistron.common.AbstractTest;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule;
import com.wistron.license.service.EncryptService;

@Ignore
public class EncryptServiceImplFalseTest extends AbstractTest{
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Mock
	private SecurityModule securityModule;

	@InjectMocks
	@Autowired
	private EncryptService encryptService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	@Test
	public void testIsActivated() {
		when(this.securityModule.isActivated()).thenReturn(new Byte("1"));
		assertTrue(this.encryptService.isActivated());

	}

	@Test
	public void testGetUniqueId() {

		assertNotNull(this.encryptService.getUniqueId());

	}

	@Test
	public void testInitModule() {

		assertTrue(this.encryptService.initModule("code", "sn"));

	}

	@Test
	public void testGetUsageCount() {

		assertEquals(10L, this.encryptService.getUsageCount());
	}

	@Test
	public void testGetUsageSign() {

		assertEquals(100, this.encryptService.getUsageSign("userid", "faceImage").getCount());

	}

	@Test
	public void testAddPurchase() {
		assertEquals(100L, this.encryptService.addPurchase("sn"));
	}

	@Test
	public void testMigrate() throws Exception {
		assertTrue(this.encryptService.migrate("token"));
	}

	@Test
	public void testGetMigrationInfo() throws Exception {

		assertEquals(StringUtils.EMPTY, this.encryptService.getMigrationInfo());

	}

	@Test
	public void testGetPurchaseList() {

		assertNull(this.encryptService.getPurchaseList());

	}

	@Test
	public void testGetChargebackHistory() {
		assertNull(this.encryptService.getChargebackHistory(100l, 110l));

	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
