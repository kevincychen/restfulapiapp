
package com.wistron.license.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import com.google.gson.Gson;
import com.sun.jna.Pointer;
import com.wistron.common.AbstractTest;
import com.wistron.license.securityModule.util.ChargeLog;
import com.wistron.license.securityModule.util.Log;
import com.wistron.license.securityModule.util.Purchase;
import com.wistron.license.securityModule.util.PurchaseHistory;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule.GoString;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule.GolibUtils;
import com.wistron.license.service.EncryptService;

@Ignore
@ActiveProfiles(profiles = "dev,true")
public class EncryptServiceImplTest extends AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	@Mock
	private SecurityModule securityModule;

	@InjectMocks
	@Autowired
	private EncryptService encryptService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	@Test
	public void testIsActivated() {
		when(this.securityModule.isActivated()).thenReturn(new Byte("1"));
		assertTrue(this.encryptService.isActivated());

	}

	@Test
	public void testGetUniqueId() {
		Pointer pointer = Mockito.mock(Pointer.class);
		when(this.securityModule.getUniqueId()).thenReturn(pointer);
		assertNull(this.encryptService.getUniqueId());

	}

	@Test
	public void testInitModule() {
		when(this.securityModule.initModule(any(GoString.ByValue.class), any(GoString.ByValue.class)))
				.thenReturn(new Byte("1"));
		assertTrue(this.encryptService.initModule("code", "sn"));

	}

	@Test
	public void testGetUsageCount() {
		when(this.securityModule.getUsageCount()).thenReturn(10L);
		assertEquals(10L, this.encryptService.getUsageCount());
	}

	@Test
	public void testGetUsageSign() {
		Pointer pointer = Mockito.mock(Pointer.class);
		when(this.securityModule.getUsageSign(any(GoString.ByValue.class), any(GoString.ByValue.class)))
				.thenReturn(pointer);
		assertNull(this.encryptService.getUsageSign("userid", "faceImage"));

	}

	@Test
	public void testAddPurchase() {
		when(this.securityModule.addPurchase(any(GoString.ByValue.class))).thenReturn(10L);
		assertEquals(10L, this.encryptService.addPurchase("sn"));
	}

	@Test
	public void testMigrate() throws Exception {
		when(this.securityModule.migrate(any(GoString.ByValue.class))).thenReturn(new Byte("1"));
		assertTrue(this.encryptService.migrate("token"));
	}

	@Test
	public void testGetMigrationInfo() throws Exception {
		Pointer pointer = Mockito.mock(Pointer.class);

		when(this.securityModule.getMigrateInfo()).thenReturn(pointer);
		assertEquals(StringUtils.EMPTY, this.encryptService.getMigrationInfo());

	}

	@Test
	public void testGetPurchaseList() {
		List<Purchase> purchases = new ArrayList<>();
		Purchase p1 = new Purchase();
		p1.setCount(10);
		p1.setSn("123");
		purchases.add(p1);
		PurchaseHistory history = new PurchaseHistory();
		history.setStatus(true);
		history.setList(purchases);
		Gson gson = new Gson();
		String returnStr = gson.toJson(history);
		// Pointer pointer = Mockito.mock(Pointer.class);
		GoString.ByValue g = GolibUtils.jstr2gstr(returnStr.length() + "|" + returnStr);
		when(this.securityModule.getPurchaseList()).thenReturn(g.p);
		PurchaseHistory newHistory = this.encryptService.getPurchaseList();
		assertTrue(newHistory.isStatus());

	}

	@Test
	public void testGetChargebackHistory() {
		Log log1 = new Log();
		log1.setSign("123");
		log1.setTimestamp(123456l);
		Log log2 = new Log();
		log2.setSign("123");
		log2.setTimestamp(123456l);
		List<Log> logs = new ArrayList<Log>();
		logs.add(log1);
		logs.add(log2);
		ChargeLog chargeLog = new ChargeLog();
		chargeLog.setList(logs);
		chargeLog.setStatus(true);

		Gson gson = new Gson();
		String returnStr = gson.toJson(chargeLog);

		GoString.ByValue g = GolibUtils.jstr2gstr(returnStr.length() + "|" + returnStr);
		when(this.securityModule.getChargebackHistory(anyLong(), anyLong())).thenReturn(g.p);
		ChargeLog cl = this.encryptService.getChargebackHistory(100l, 110l);
		assertTrue(cl.isStatus());

	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
