
package com.wistron.license.service.impl;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.wistron.common.AbstractTest;
import com.wistron.license.common.ActivationInfoVO;
import com.wistron.license.service.HttpService;

public class HttpServiceImplTest extends AbstractTest {
	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====

	@Mock
	private CloseableHttpClient httpClient = HttpClients.createDefault();

	@InjectMocks
	@Autowired
	private HttpService httpService;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	@Test
	public void testSendCodeToServer() throws ClientProtocolException, IOException {
		ActivationInfoVO requestVO = new ActivationInfoVO("123", "456", "789");
		CloseableHttpResponse response = Mockito.mock(CloseableHttpResponse.class);
		HttpEntity httpEntity = Mockito.mock(HttpEntity.class);
		when(this.httpClient.execute(any(HttpPost.class))).thenReturn(response);
		when(response.getEntity()).thenReturn(httpEntity);
		assertNull(this.httpService.sendCodeToServer(requestVO));

	}
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================
}
