package com.wistron.license.common.util;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.junit.*;


public class ReaderAndWriterFileUtilsTest {

	private static String testData = "test data is important";
	
	@BeforeClass
	public static void createFile() {
		
		Path path = Paths.get("UTTestRead.txt");
		if (!Files.exists(path)) {
			try {
				FileUtils.writeStringToFile(path.toFile(), testData, "UTF-8");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void testReaderFile() {
		
		try {
			String ret = ReaderAndWriterFileUtils.readerFile("UTTestRead.txt");
			assertTrue(ret.equalsIgnoreCase(testData));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	public void testWriterFile() {
		ReaderAndWriterFileUtils.writerFile(testData, "UTTestWrite.txt");
		assertTrue(Files.exists(Paths.get("UTTestWrite.txt")));
	}

	@AfterClass
	public static void removeFile() {
		Path path = Paths.get("UTTestRead.txt");
		try {
			FileUtils.forceDelete(path.toFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		path = Paths.get("UTTestWrite.txt");
		try {
			FileUtils.forceDelete(path.toFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
