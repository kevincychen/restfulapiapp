package com.wistron.license.common.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class FileVOTest {

	@Test
	public void test() {
		FileVO f = new FileVO("test", "test", "test", "test");
		f.setFaceImage("test1");
		assertTrue(f.getFaceImage().equalsIgnoreCase("test1"));
		f.setSign("test1");
		assertTrue(f.getSign().equalsIgnoreCase("test1"));
	}

}
