package com.wistron.license.common.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DateUtilsTest {
	
	private static Calendar calendar = Calendar.getInstance();

	@Test
	public void testGetDateStartSuccess() {
		Date now = new Date();
		Date start = DateUtils.getDateStart(now);
		assertTrue(start != null);
	}
	
	@Test
	public void testGetDateStartFail() {
		Date start = DateUtils.getDateStart(null);
		assertTrue(start == null);
	}

	@Test
	public void testGetDateEndSuccess() {
		Date now = new Date();
		Date end = DateUtils.getDateEnd(now);
		assertTrue(end != null);
	}
	
	@Test
	public void testGetDateEndFail() {
		Date end = DateUtils.getDateEnd(null);
		assertTrue(end == null);
	}

	@Test
	public void testGetTodayStr() {
		
		int year = calendar.get(Calendar.YEAR);
		String ret = DateUtils.getTodayStr("YYYY");
		assertTrue(String.valueOf(year).equalsIgnoreCase(ret));
	}

	@Test
	public void testGetDateStrDateString() {
		Date now = new Date();
		String ret = DateUtils.getDateStr(now, "YYYYMMDD");
		assertTrue(ret != null);
	}

	@Test
	public void testGetDateStrTimestampString() {
		Timestamp now = new Timestamp(new Date().getTime());
		String ret = DateUtils.getDateStr(now, "YYYYMMDD");
		assertTrue(ret != null);
	}

	@Test
	public void testGetDateStrLongString() {
		Date now = new Date();
		String ret = DateUtils.getDateStr(now.getTime(), "YYYYMMDD");
		assertTrue(ret != null);
	}

	@Test
	public void testAddDay() {
		String day1 = "20180101";
		String format = "YYYYMMDD";
		int addDay = 3;
		String ret;
		try {
			ret = DateUtils.addDay(day1, format, addDay);
			assertTrue(ret.equalsIgnoreCase("20180103"));
		} catch (ParseException e) {
			fail("Catch exception"+e.getMessage());
		}
		
	}

}
