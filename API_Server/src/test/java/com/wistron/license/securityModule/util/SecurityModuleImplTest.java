package com.wistron.license.securityModule.util;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule.GoString;
import com.wistron.license.securityModule.util.SecurityModuleImpl.SecurityModule.GolibUtils;

@Ignore
public class SecurityModuleImplTest {

	@Test
	public void test() {
		GoString.ByValue g = GolibUtils.jstr2gstr("9|Test data");
		String result = GolibUtils.gstr2jstr(g.p); 
		assertTrue(result.equalsIgnoreCase("Test data"));
	}

}
