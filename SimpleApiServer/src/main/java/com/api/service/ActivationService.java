package com.api.service;

import com.api.common.ActivationInfoVO;
import com.api.common.DefaultResponseVO;

public interface ActivationService {
  DefaultResponseVO activate(ActivationInfoVO requestVO);
}
