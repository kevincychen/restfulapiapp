package com.api.service;

import com.api.common.FrsRequestVO;
import com.api.common.FrsResponseVO;

public interface FrsService {
	public FrsResponseVO recognition(FrsRequestVO requestVO);
}
