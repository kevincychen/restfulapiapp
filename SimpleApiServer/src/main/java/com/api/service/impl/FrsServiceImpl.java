package com.api.service.impl;

import org.springframework.stereotype.Service;

import com.api.common.Face;
import com.api.common.FrsRequestVO;
import com.api.common.FrsResponseVO;
import com.api.common.Quality;
import com.api.common.ResponseMsg;
import com.api.service.FrsService;

@Service
public class FrsServiceImpl implements FrsService {

	@Override
	public FrsResponseVO recognition(FrsRequestVO requestVO) {
		FrsResponseVO responseVO = new FrsResponseVO();
		Face tempFace = new Face();
		Quality quality = new Quality();
		quality.setBrightness((float)(1.6));
		quality.setSharpness((float)(1.6));
		tempFace.setQuality(quality);
		tempFace.setSimilarity((float)(0.8));
		responseVO.setFace(tempFace);
		responseVO.setTimestamp(1522357140);
		responseVO.setUserID("10505302");
		responseVO.setMessage(ResponseMsg.OK.toString());
		responseVO.setSize((float)(15.8));
		return responseVO;
	}

}
