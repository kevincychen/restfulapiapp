package com.api.service.impl;

import org.springframework.stereotype.Service;

import com.api.common.ActivationInfoVO;
import com.api.common.DefaultResponseVO;
import com.api.common.ResponseMsg;
import com.api.service.ActivationService;


@Service
public class ActivationServiceImpl implements ActivationService {

	@Override
	public DefaultResponseVO activate(ActivationInfoVO requestVO) {
		return new DefaultResponseVO(ResponseMsg.OK);
	}

}
