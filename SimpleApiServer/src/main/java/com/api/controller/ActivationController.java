package com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.common.ActivationInfoVO;
import com.api.common.DefaultResponseVO;
import com.api.service.ActivationService;


@RestController
public class ActivationController {
	@Autowired
	private ActivationService activationService;
	
	@RequestMapping(value = "/activate", method = RequestMethod.POST, produces = {"application/json"})  
	public DefaultResponseVO activate(@RequestBody ActivationInfoVO requestVO) {
		return activationService.activate(requestVO);
	}
}
