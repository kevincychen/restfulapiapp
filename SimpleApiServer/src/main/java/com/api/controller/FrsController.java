package com.api.controller;

import static com.api.common.FrsAction.RECOGNITION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.common.FrsRequestVO;
import com.api.common.FrsResponseVO;
import com.api.service.FrsService;

@RestController
@RequestMapping("/frs")
@CrossOrigin
public class FrsController {

	@Autowired
	private FrsService frsService;
	
	@RequestMapping(value = "/recognition", method = RequestMethod.POST, produces = {"application/json"})  
	public FrsResponseVO recognition(@RequestBody FrsRequestVO requestVO) {
		requestVO.setAction(RECOGNITION.getValue());
		return this.frsService.recognition(requestVO);

	}
}
