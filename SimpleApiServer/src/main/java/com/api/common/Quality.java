
package com.api.common;

import java.io.Serializable;

public class Quality implements Serializable {

	// ================================================
	// == [Enumeration types] Block Start
	// ====
	// ====
	// == [Enumeration types] Block End
	// ================================================
	// == [static variables] Block Start
	// ====
	/**
	 * <code>serialVersionUID</code> 的註�?
	 */
	private static final long serialVersionUID = 1605721146036703830L;
	// ====
	// == [static variables] Block Stop
	// ================================================
	// == [instance variables] Block Start
	// ====
	private float brightness;

	private float sharpness;

	// ====
	// == [instance variables] Block Stop
	// ================================================
	// == [static Constructor] Block Start
	// ====
	// ====
	// == [static Constructor] Block Stop
	// ================================================
	// == [Constructors] Block Start (含init method)
	// ====
	// ====
	// == [Constructors] Block Stop
	// ================================================
	// == [Static Method] Block Start
	// ====
	// ====
	// == [Static Method] Block Stop
	// ================================================
	// == [Accessor] Block Start
	// ====
	public float getBrightness() {
		return this.brightness;
	}

	public void setBrightness(float brightness) {
		this.brightness = brightness;
	}

	public float getSharpness() {
		return this.sharpness;
	}

	public void setSharpness(float sharpness) {
		this.sharpness = sharpness;
	}
	// ====
	// == [Accessor] Block Stop
	// ================================================
	// == [Overrided Method] Block Start (Ex. toString/equals+hashCode)
	// ====
	// ====
	// == [Overrided Method] Block Stop
	// ================================================
	// == [Method] Block Start
	// ====
	// ####################################################################
	// ## [Method] sub-block :
	// ####################################################################
	// ====
	// == [Method] Block Stop
	// ================================================

}
