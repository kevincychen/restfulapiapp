
package com.api.common;

public enum FrsStatus {
	SUCCESS(0), //
	USER_ID_ERROR(-1), //
	NOT_ACTIVATED(-2), //
	PARAMS_ERROR(-3), //
	NOT_ENOUGH(-4), //
	FILE_TOO_LARGE(-5), //
	SERVER_ERROR(-6); //

	private int value;

	public int getValue() {
		return this.value;
	}

	FrsStatus(int value) {
		this.value = value;
	}
}
