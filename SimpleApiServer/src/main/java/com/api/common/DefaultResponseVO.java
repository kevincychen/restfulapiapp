package com.api.common;

public class DefaultResponseVO extends BasicVO {

	private static final long serialVersionUID = 8612613338591121166L;

	private String message;

	public DefaultResponseVO() {
	}

	public DefaultResponseVO(ResponseMsg responseMsg) {
		if (responseMsg != null) {
			this.message = responseMsg.getValue();
		}

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
