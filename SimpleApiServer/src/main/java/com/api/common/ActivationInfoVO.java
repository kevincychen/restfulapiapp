package com.api.common;

public class ActivationInfoVO extends BasicVO {

	/**
	 * <code>serialVersionUID</code> 的註�?
	 */
	private static final long serialVersionUID = 8612613338591121166L;
	private String projectID;
	private String dockerID;
	private String code;

	public ActivationInfoVO() {
	}

	public ActivationInfoVO(String projectID, String dockerID, String code) {
		super();
		this.projectID = projectID;
		this.dockerID = dockerID;
		this.code = code;
	}

	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public String getDockerID() {
		return dockerID;
	}

	public void setDockerID(String dockerID) {
		this.dockerID = dockerID;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
